# Static files middleware

## Usage

`StaticFileMiddleware` implements `Psr\Http\Server\MiddlewareInterface` and
can be used as a middleware as in [PSR-15](https://www.php-fig.org/psr/psr-15/) recommendantions.

## Congfiguration

This middleware is configured with [`StaticFileOptions`](../src/StaticFileOptions.php).

The class [`StaticFileMiddleware`](../src/StaticFileMiddleware.php) is the middleware implementation.

- `map(string, string)`: map the prefix path of a request to a directory.
- `addMimeType(string, string)`: map a file extension to a content type.
- `removeMimeType(string)`: remove the mapping for an extension.
- `serveFileWithoutMimeType(string)`: serve file without mime-type with a default one.

## How it works ?

![diagram](./static-files.png)
