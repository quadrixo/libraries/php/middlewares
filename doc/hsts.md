# HSTS middleware

## Usage

`HstsMiddleware` implements `Psr\Http\Server\MiddlewareInterface` and
can be used as a middleware as in [PSR-15](https://www.php-fig.org/psr/psr-15/) recommendantions.

## Congfiguration

This middleware is configured with [`HstsOptions`](../src/HstsOptions.php).

The class [`HstsMiddleware`](../src/HstsMiddleware.php) is the middleware extension.

- `setMaxAge(int)`: set max-age parameter in seconds of the Strict-Transport-Security header (default: 2592000/30 days).
- `setIncludeSubDomains(bool)`: enables includeSubDomain parameter of the Strict-Transport-Security header (default: false).
- `setPreload(bool)`: set the preload parameter of the Strict-Transport-Security header (default: false).
- `setExcludedHosts(string[])`: set the list of host names that will not add the HSTS header (default: ['localhost', '127.0.0.1', '[::1]']).
- `addExcludedHosts(string|array)`: add host names that will not add the HSTS header.

## How it works ?

![diagram](./hsts.png)
