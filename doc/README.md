# quadrixo/middlewares

## List of middlewares

- [HttpsRedirectionMiddleware](./https-redirection.md): redirect HTTP request to HTTPS.
- [StaticFileMiddleware](./static-files.md): serve static files.
