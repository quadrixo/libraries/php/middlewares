# HTTPS Redirection middleware

## Usage

`HttpsRedirectionMiddleware` implements `Psr\Http\Server\MiddlewareInterface` and
can be used as a middleware as in [PSR-15](https://www.php-fig.org/psr/psr-15/) recommendantions.

## Congfiguration

This middleware is configured with [`HttpsRedirectionOptions`](../src/HttpsRedirectionOptions.php).

The implementation of the middleware is the class [`HttpsRedirectionMiddleware`](../src/HttpsRedirectionMiddleware.php).

- `setStatusCode(int)`: specify the HTTP status code for the redirection (default: 307).
- `setHttpsPort(int)`: specify the HTTPS port (default: 443).

## How it works ?

![diagram](./https-redirection.png)
