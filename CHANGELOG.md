# Version 0.7.3 - 2021-09-16
## Added
- Add `getParsedBody()` to `AbstractPage` and `ApiControllerBase`

# Version 0.7.2 - 2021-09-10
## Fixed
- Fix CORS middleware

# Version 0.7.1 - 2021-09-10
## Added
- Add CORS middleware

# Version 0.7.0 - 2021-09-08
## Changed
- Adapt to the removing of `AppContext`

# Version 0.6.1 - 2021-09-06
## Fixed
- Fix unit tests

# Version 0.6.0 - 2021-09-06
## Changed
- Rename `Quadrixo\StaticFileOptions` and `Quadrixo\StaticFileMiddleware` into
  `Quadrixo\StaticFilesOptions` and `Quadrixo\StaticFilesMiddleware`
- Updates dependencies

# Version 0.5.1 - 2021-08-10
## Fixed
- Bad interpretation for ResultInterface returned by action of crontroller

# Version 0.5.0 - 2021-08-09
## Added
- Add `WebApiMiddleware` wich map a route to its handler, based on `nikic/fast-route`

# Version 0.4.1 - 2021-08-09
## Change
- `AbstractPage` use `AppContextAwaireTrait` to get AppContext and Contaier
- Refactor `PlatesPageMiddleware`

# Version 0.4.0 - 2021-08-05
## Change
- Drop support for PHP < 7.3
- Remove the use of web-application in unit tests

# Version 0.3.0 - 2021-07-31
## Added
- Add plates page middleware
- Add unit tests to HTTPS and HSTS middlewares
## Changed
- Refactor static file middleware

# Version 0.2.1 - 2021-03-31
## Added
- Add HTTPS redirection middleware
- Add HSTS middleware

# Version 0.2.0 - 2021-03-31
## Added
- Possibility to force/override mime-type for a file extension
## Changed
- Mapping to a directory returns 404 not instead of calling next middleware

# Version 0.1.0 - 2021-03-30
## Added
- `Quadrixo\Middlewares\StaticFileMiddleware` which serves static files
