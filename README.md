[![pipeline status](https://gitlab.com/quadrixo/libraries/php/web-application/badges/main/pipeline.svg)](https://gitlab.com/quadrixo/libraries/php/web-application/-/commits/main)  [![coverage report](https://gitlab.com/quadrixo/libraries/php/web-application/badges/main/coverage.svg)](https://gitlab.com/quadrixo/libraries/php/web-application/-/commits/main)

# quadrixo/middlewares

`quadrixo/middlewares` is a library containing middlewares comptible with PSR-15.

Theses middlewares are designed to suit well with [quadrixo\web-application](https://gitlab.com/quadrixo/libraries/php/web-application), but not only.

## License

This software is licensed under the [CeCILL](https://cecill.info/) license.

[CeCILL-2.1](./LICENSE)
