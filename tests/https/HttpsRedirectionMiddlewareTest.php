<?php
declare(strict_types = 1);
namespace Quadrixo\HstsMiddlewareTest;

use Fig\Http\Message\RequestMethodInterface as RequestMethod;
use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7Server\ServerRequestCreator;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Quadrixo\Middlewares\HstsMiddleware;
use Quadrixo\Middlewares\HstsOptions;

class HstsMiddlewareTest extends TestCase
{
    /**
     * @dataProvider provideTestHeaderData
     */
    public function testHeader(int $maxAge, bool $includeSubDomaions, bool $preload, string $expected)
    {
            $factory = new Psr17Factory();

            $options = (new HstsOptions())
                ->setMaxAge($maxAge)
                ->setIncludeSubDomains($includeSubDomaions)
                ->setPreload($preload);
            $handler = new HstsMiddleware($options);
            $next = new class() implements RequestHandlerInterface
            {
                public function handle(ServerRequestInterface $request): ResponseInterface
                {
                    return (new Psr17Factory())->createResponse(404);
                }
            };

            $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
            $request = $creator->fromArrays([
                'HTTPS' => 'on',
                'REQUEST_METHOD' => RequestMethod::METHOD_GET,
                'REQUEST_URI' => '/'
            ]);
            $response = $handler->process($request, $next);

            $this->assertEquals($expected, $response->getHeaderLine('Strict-Transport-Security'));
    }

    /**
     * @dataProvider provideHosts
     */
    public function testDoNotSetHeaderForDefaultExcludedHosts(string $host)
    {
            $factory = new Psr17Factory();

            $options = (new HstsOptions())->addExcludedHosts('somewhere.local');
            $handler = new HstsMiddleware($options);
            $next = new class() implements RequestHandlerInterface
            {
                public function handle(ServerRequestInterface $request): ResponseInterface
                {
                    $factory = new Psr17Factory();
                        return $factory->createResponse()
                            ->withBody($factory->createStream('Hello World !'));
                }
            };

            $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
            $request = $creator->fromArrays([
                'HTTPS' => 'on',
                'HTTP_HOST' => $host,
                'REQUEST_METHOD' => RequestMethod::METHOD_GET,
                'REQUEST_URI' => '/'
            ]);
            $response = $handler->process($request, $next);

            $this->assertFalse($response->hasHeader('Strict-Transport-Security'));
    }

    /**
     * @dataProvider provideHosts
     */
    public function testSetHeaderForEmptyExcludedHosts(string $host)
    {
            $factory = new Psr17Factory();

            $options = (new HstsOptions())->setExcludedHosts([]);
            $handler = new HstsMiddleware($options);
            $next = new class() implements RequestHandlerInterface
            {
                public function handle(ServerRequestInterface $request): ResponseInterface
                {
                    $factory = new Psr17Factory();
                        return $factory->createResponse()
                            ->withBody($factory->createStream('Hello World !'));
                }
            };

            $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
            $request = $creator->fromArrays([
                'HTTPS' => 'on',
                'HTTP_HOST' => $host,
                'REQUEST_METHOD' => RequestMethod::METHOD_GET,
                'REQUEST_URI' => '/'
            ]);
            $response = $handler->process($request, $next);

            $this->assertTrue($response->hasHeader('Strict-Transport-Security'));
    }

    public function provideTestHeaderData(): array
    {
        return [
            [ 0, false, false, 'max-age=0' ],
            [ -1, false, false, 'max-age=-1' ],
            [ 0, true, false, 'max-age=0; includeSubDomains' ],
            [ 50000, false, true, 'max-age=50000; preload' ],
            [ 0, true, true, 'max-age=0; includeSubDomains; preload' ],
            [ 50000, true, true, 'max-age=50000; includeSubDomains; preload' ]
        ];
    }

    public function provideHosts(): array
    {
        return [
            [ 'localhost' ],
            [ 'Localhost' ],
            [ 'LOCALHOST' ],
            [ '127.0.0.1' ],
            [ '[::1]' ],
            [ 'somewhere.local' ]
        ];
    }
}
