<?php
declare(strict_types = 1);
namespace Quadrixo\HttpsRedirectionMiddlewareTest;

use Fig\Http\Message\RequestMethodInterface as RequestMethod;
use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7Server\ServerRequestCreator;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Quadrixo\Middlewares\HttpsRedirectionMiddleware;
use Quadrixo\Middlewares\HttpsRedirectionOptions;

class HttpsRedirectionMiddlewareTest extends TestCase
{
    /**
     * @dataProvider provideRedirectionValues
     */
    public function testDefaultRedirection(int $statusCode, int $httpsPort, $expected)
    {
            $factory = new Psr17Factory();

            $options = (new HttpsRedirectionOptions())
                ->setHttpsPort($httpsPort)
                ->setStatusCode($statusCode);
            $handler = new HttpsRedirectionMiddleware($options, $factory);
            $next = new class() implements RequestHandlerInterface
            {
                public function handle(ServerRequestInterface $request): ResponseInterface
                {
                    return (new Psr17Factory())->createResponse(404);
                }
            };

            $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
            $request = $creator->fromArrays([
                'REQUEST_METHOD' => RequestMethod::METHOD_GET,
                'HTTP_HOST' => 'localhost',
                'REQUEST_URI' => '/'
            ]);
            $response = $handler->process($request, $next);

            $this->assertEquals($statusCode, $response->getStatusCode());
            $this->assertEquals($expected, $response->getHeaderLine('Location'));
    }

    public function provideRedirectionValues(): array
    {
        return [
            [ 302, 5001, 'https://localhost:5001/' ],
            [ 307, 1, 'https://localhost:1/' ],
            [ 308, 3449, 'https://localhost:3449/' ],
            [ 301, 5050, 'https://localhost:5050/' ],
            [ 301, 443, 'https://localhost/' ]
        ];
    }
}
