<?php
declare(strict_types = 1);
namespace Quadrixo\CorsTest;

use Fig\Http\Message\RequestMethodInterface as RequestMethods;
use InvalidArgumentException;
use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7Server\ServerRequestCreator;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Quadrixo\Middlewares\CorsOptions;
use Quadrixo\Middlewares\Internal\CorsPolicy;

class CorsTest extends TestCase
{
    /** @var Psr17Factory */
    private $factory;
    /** @var ServerRequestCreator */
    private $creator;
    /** @var RequestHandlerInterface */
    private $ok;

    protected function setUp(): void
    {
        $this->factory = new Psr17Factory();
        $this->creator = new ServerRequestCreator($this->factory, $this->factory, $this->factory, $this->factory);
        $this->ok = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(200);
            }
        };
    }

    public function testExceptionWithInsecureConfiguration()
    {
        $options = (new CorsOptions())->allowAnyOrigin()->allowCredentials();
        $policy = new CorsPolicy($options);
        $request = $this->creator->fromArrays([
            'REQUEST_METHOD' => RequestMethods::METHOD_POST,
            'HTTP_HOST' => 'localhost',
            'REQUEST_URI' => '/'
        ]);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The CORS protocol does not allow specifying a wildcard (any) origin and credentials at the same time. Configure the CORS policy by listing individual origins if credentials needs to be supported.');
        $policy->evaluate($request, $this->ok);
    }

    public function testEmptyOriginsPolicy()
    {
        $options = (new CorsOptions());
        $policy = new CorsPolicy($options);
        $request = $this->creator->fromArrays(
            [
                'REQUEST_METHOD' => RequestMethods::METHOD_GET,
            ],
            [
                'Origin' => 'http://example.com'
            ]
        );

        list($isPreflight, $allowedOrigin, $headers, $varyByOrigin) = $policy->evaluate($request, $this->ok);
        $this->assertNull($allowedOrigin);
    }

    public function testOriginNotAllowed()
    {
        $options = (new CorsOptions())->withOrigins('example.com');
        $policy = new CorsPolicy($options);
        $request = $this->creator->fromArrays(
            [
                'REQUEST_METHOD' => RequestMethods::METHOD_GET,
            ],
            [
                'Origin' => 'http://example.com'
            ]
        );

        list($isPreflight, $allowedOrigin, $headers, $varyByOrigin) = $policy->evaluate($request, $this->ok);
        $this->assertNull($allowedOrigin);
    }

    public function testAllowAnyOrigin()
    {
        $options = (new CorsOptions())->allowAnyOrigin();
        $policy = new CorsPolicy($options);
        $request = $this->creator->fromArrays(
            [
                'REQUEST_METHOD' => RequestMethods::METHOD_GET,
            ],
            [
                'Origin' => 'http://example.com'
            ]
        );

        list($isPreflight, $allowedOrigin, $headers, $varyByOrigin) = $policy->evaluate($request, $this->ok);
        $this->assertEquals('*', $allowedOrigin);
    }

    public function testAllowOneOrigin()
    {
        $options = (new CorsOptions())->withOrigins('http://example.com');
        $policy = new CorsPolicy($options);
        $request = $this->creator->fromArrays(
            [
                'REQUEST_METHOD' => RequestMethods::METHOD_GET,
            ],
            [
                'Origin' => 'http://example.com'
            ]
        );

        list($isPreflight, $allowedOrigin, $headers, $varyByOrigin) = $policy->evaluate($request, $this->ok);
        $this->assertEquals('http://example.com', $allowedOrigin);
        $this->assertFalse($varyByOrigin);
    }

    public function testAllowMultipleOrigins()
    {
        $options = (new CorsOptions())->withOrigins('http://example.com', 'http://example.net');
        $policy = new CorsPolicy($options);
        $request = $this->creator->fromArrays(
            [
                'REQUEST_METHOD' => RequestMethods::METHOD_GET,
            ],
            [
                'Origin' => 'http://example.com'
            ]
        );

        list($isPreflight, $allowedOrigin, $headers, $varyByOrigin) = $policy->evaluate($request, $this->ok);
        $this->assertEquals('http://example.com', $allowedOrigin);
        $this->assertTrue($varyByOrigin);
    }

    public function testNotPreflightRequest()
    {
        $options = (new CorsOptions())->allowAnyOrigin();
        $policy = new CorsPolicy($options);
        $request = $this->creator->fromArrays(
            [
                'REQUEST_METHOD' => RequestMethods::METHOD_OPTIONS,
            ],
            [
                'Origin' => 'http://example.com'
            ]
        );

        list($isPreflight, $allowedOrigin, $headers, $varyByOrigin) = $policy->evaluate($request, $this->ok);
        $this->assertFalse($isPreflight);
    }

    public function testPreflightRequest()
    {
        $options = (new CorsOptions())->allowAnyOrigin();
        $policy = new CorsPolicy($options);
        $request = $this->creator->fromArrays(
            [
                'REQUEST_METHOD' => RequestMethods::METHOD_OPTIONS,
            ],
            [
                'Origin' => 'http://example.com',
                'Access-Control-Request-Method' => RequestMethods::METHOD_POST
            ]
        );

        list($isPreflight, $allowedOrigin, $headers, $varyByOrigin) = $policy->evaluate($request, $this->ok);
        $this->assertTrue($isPreflight);
    }
}
