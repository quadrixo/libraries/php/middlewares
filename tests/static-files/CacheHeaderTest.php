<?php
declare(strict_types = 1);
namespace Quadrixo\CacheHeaderTest;

use BadMethodCallException;
use Fig\Http\Message\RequestMethodInterface as RequestMethod;
use Fig\Http\Message\StatusCodeInterface as StatusCode;
use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7Server\ServerRequestCreator;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Quadrixo\Middlewares\StaticFilesMiddleware;
use Quadrixo\Middlewares\StaticFilesOptions;

class CacheHeaderTest extends TestCase
{
    public function testETagOnFoundFile()
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())->map('/', __DIR__);
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $request = $creator->fromArrays([
            'REQUEST_METHOD' => RequestMethod::METHOD_GET,
            'REQUEST_URI' => '/test.txt'
        ]);
        $response = $handler->process($request, $next);

        $this->assertTrue($response->hasHeader('ETag'));
    }

    public function testSameETagGenetation()
    {
        $factory = new Psr17Factory();

        $options = new StaticFilesOptions();
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $req1 = $creator->fromArrays([
            'REQUEST_METHOD' => RequestMethod::METHOD_GET,
            'REQUEST_URI' => '/test.txt'
        ]);
        $req2 = $creator->fromArrays([
            'REQUEST_METHOD' => RequestMethod::METHOD_GET,
            'REQUEST_URI' => '/test.txt'
        ]);

        $resp1 = $handler->process($req1, $next);
        $resp2 = $handler->process($req2, $next);

        $this->assertEquals($resp1->getHeaderLine('ETag'), $resp2->getHeaderLine('ETag'));
    }

    /**
     * @dataProvider provideAllowedMethods
     */
    public function testIfMatch412(string $method)
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())->map('/', __DIR__);
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $request = $creator->fromArrays([
            'REQUEST_METHOD' => $method,
            'REQUEST_URI' => '/test.txt'
        ])
        ->withHeader('If-Match', '"fake"');
        $response = $handler->process($request, $next);

        $this->assertEquals(StatusCode::STATUS_PRECONDITION_FAILED, $response->getStatusCode());
    }

    /**
     * @dataProvider provideAllowedMethods
     */
    public function testIfMatch200(string $method)
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())->map('/', __DIR__);
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $req1 = $creator->fromArrays([
            'REQUEST_METHOD' => $method,
            'REQUEST_URI' => '/test.txt'
        ]);
        $resp1 = $handler->process($req1, $next);

        $req2 = $creator->fromArrays([
            'REQUEST_METHOD' => $method,
            'REQUEST_URI' => '/test.txt'
        ])
        ->withHeader('If-Match', $resp1->getHeaderLine('ETag'));
        $resp2 = $handler->process($req2, $next);

        $this->assertEquals(StatusCode::STATUS_OK, $resp2->getStatusCode());
    }

    /**
     * @dataProvider provideAllowedMethods
     */
    public function testIfMatchAsterisk200(string $method)
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())->map('/', __DIR__);
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $request = $creator->fromArrays([
            'REQUEST_METHOD' => $method,
            'REQUEST_URI' => '/test.txt'
        ])
        ->withHeader('If-Match', '*');
        $response = $handler->process($request, $next);

        $this->assertEquals(StatusCode::STATUS_OK, $response->getStatusCode());
    }

    /**
     * @dataProvider provideDisallowedMethods
     */
    public function testIfMatchIgnored(string $method)
    {
        $factory = new Psr17Factory();

        $options = new StaticFilesOptions();
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $request = $creator->fromArrays([
            'REQUEST_METHOD' => $method,
            'REQUEST_URI' => '/test.txt'
        ])
        ->withHeader('If-Match', '"fake"');
        $response = $handler->process($request, $next);

        $this->assertEquals(StatusCode::STATUS_NOT_FOUND, $response->getStatusCode());
    }

    /**
     * @dataProvider provideAllowedMethods
     */
    public function testIfNoneMatch304(string $method)
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())->map('/', __DIR__);
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $req1 = $creator->fromArrays([
            'REQUEST_METHOD' => $method,
            'REQUEST_URI' => '/test.txt'
        ]);
        $resp1 = $handler->process($req1, $next);

        $req2 = $creator->fromArrays([
            'REQUEST_METHOD' => $method,
            'REQUEST_URI' => '/test.txt'
        ])
        ->withHeader('If-None-Match', $resp1->getHeaderLine('ETag'));
        $resp2 = $handler->process($req2, $next);

        $this->assertEquals(StatusCode::STATUS_NOT_MODIFIED, $resp2->getStatusCode());
    }

    /**
     * @dataProvider provideAllowedMethods
     */
    public function testIfNoneMatchAsterisk304(string $method)
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())->map('/', __DIR__);
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $request = $creator->fromArrays([
            'REQUEST_METHOD' => $method,
            'REQUEST_URI' => '/test.txt'
        ])
        ->withHeader('If-None-Match', '*');
        $response = $handler->process($request, $next);

        $this->assertEquals(StatusCode::STATUS_NOT_MODIFIED, $response->getStatusCode());
    }

    /**
     * @dataProvider provideDisallowedMethods
     */
    public function testIfNoneMatchIgnored(string $method)
    {
        $factory = new Psr17Factory();

        $options = new StaticFilesOptions();
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $req1 = $creator->fromArrays([
            'REQUEST_METHOD' => RequestMethod::METHOD_GET,
            'REQUEST_URI' => '/test.txt'
        ]);
        $resp1 = $handler->process($req1, $next);

        $req2 = $creator->fromArrays([
            'REQUEST_METHOD' => $method,
            'REQUEST_URI' => '/test.txt'
        ])
        ->withHeader('If-None-Match', $resp1->getHeaderLine('ETag'));
        $resp2 = $handler->process($req2, $next);

        $this->assertEquals(StatusCode::STATUS_NOT_FOUND, $resp2->getStatusCode());
    }

    /**
     * @dataProvider provideAllowedMethods
     */
    public function testReturnLastModified(string $method)
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())->map('/', __DIR__);
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $request = $creator->fromArrays([
            'REQUEST_METHOD' => $method,
            'REQUEST_URI' => '/test.txt'
        ]);
        $response = $handler->process($request, $next);

        $this->assertTrue($response->hasHeader('Last-Modified'));
        $this->assertGreaterThan(0, strtotime($response->getHeaderLine('Last-Modified')));
    }

    /**
     * @dataProvider provideAllowedMethods
     */
    public function testMatchingBothConditionsReturnsNotModified(string $method)
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())->map('/', __DIR__);
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $req1 = $creator->fromArrays([
            'REQUEST_METHOD' => RequestMethod::METHOD_GET,
            'REQUEST_URI' => '/test.txt'
        ]);
        $resp1 = $handler->process($req1, $next);

        $req2 = $creator->fromArrays([
            'REQUEST_METHOD' => $method,
            'REQUEST_URI' => '/test.txt'
        ])
        ->withHeader('If-None-Match', $resp1->getHeaderLine('ETag'))
        ->withHeader('If-Modified-Since', $resp1->getHeaderLine('Last-Modified'));
        $resp2 = $handler->process($req2, $next);

        $this->assertEquals(StatusCode::STATUS_NOT_MODIFIED, $resp2->getStatusCode());
    }

    public function provideAllowedMethods(): array
    {
        return [
            [ RequestMethod::METHOD_GET ],
            [ RequestMethod::METHOD_HEAD ]
        ];
    }

    public function provideDisallowedMethods(): array
    {
        return [
            [ RequestMethod::METHOD_CONNECT ],
            [ RequestMethod::METHOD_DELETE ],
            [ RequestMethod::METHOD_OPTIONS ],
            [ RequestMethod::METHOD_PATCH ],
            [ RequestMethod::METHOD_POST ],
            [ RequestMethod::METHOD_PURGE ],
            [ RequestMethod::METHOD_PUT ],
            [ RequestMethod::METHOD_TRACE ]
        ];
    }
}
