<?php
declare(strict_types = 1);
namespace Quadrixo\StaticFilesMiddlewareTest;

use Fig\Http\Message\RequestMethodInterface as RequestMethod;
use Fig\Http\Message\StatusCodeInterface as StatusCode;
use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7Server\ServerRequestCreator;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Quadrixo\Middlewares\StaticFilesMiddleware;
use Quadrixo\Middlewares\StaticFilesOptions;

class StaticFilesMiddlewareTest extends TestCase
{
    public function testOverrideMimeType()
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())->mapFile('/characters.txt', __DIR__ . '/sub-folder/characters.txt', 'application/vnd.quadrixo.text');
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $request = $creator->fromArrays([
            'REQUEST_METHOD' => RequestMethod::METHOD_GET,
            'REQUEST_URI' => '/characters.txt'
        ]);
        $response = $handler->process($request, $next);

        $this->assertEquals(StatusCode::STATUS_OK, $response->getStatusCode());
        $this->assertEquals('application/vnd.quadrixo.text', $response->getHeaderLine('Content-Type'));
    }

    public function testCustomMimeType()
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())
            ->map('/', __DIR__ . '/sub-folder')
            ->addMimeType('.qx', 'application/vnd.quadrixo.text');
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $request = $creator->fromArrays([
            'REQUEST_METHOD' => RequestMethod::METHOD_GET,
            'REQUEST_URI' => '/custom.qx'
        ]);
        $response = $handler->process($request, $next);

        $this->assertEquals(StatusCode::STATUS_OK, $response->getStatusCode());
        $this->assertEquals('application/vnd.quadrixo.text', $response->getHeaderLine('Content-Type'));
    }

    public function testRemoveMimeTypeServeDefault()
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())
            ->map('/', __DIR__ . '/sub-folder')
            ->removeMimeType('.txt')
            ->serveFileWithoutMimeType('application/vnd.quadrixo.text');
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $request = $creator->fromArrays([
            'REQUEST_METHOD' => RequestMethod::METHOD_GET,
            'REQUEST_URI' => '/characters.txt'
        ]);
        $response = $handler->process($request, $next);

        $this->assertEquals(StatusCode::STATUS_OK, $response->getStatusCode());
        $this->assertEquals('application/vnd.quadrixo.text', $response->getHeaderLine('Content-Type'));
    }

    /**
     * @dataProvider providePassesThroughRequests
     */
    public function testPassesThrough(string $method, string $requestUrl)
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())
            ->removeMimeType('.txt')
            ->serveFileWithoutMimeType('application/vnd.quadrixo.text');
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                $factory = new Psr17Factory();
                return $factory
                    ->createResponse()
                    ->withBody($factory->createStream('Hello World !'));
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $request = $creator->fromArrays([
            'REQUEST_METHOD' => $method,
            'REQUEST_URI' => $requestUrl
        ]);
        $response = $handler->process($request, $next);

        $this->assertEquals(StatusCode::STATUS_OK, $response->getStatusCode());
        $this->assertEquals('Hello World !', strval($response->getBody()));
    }

    /**
     * @dataProvider providePassesThroughFiles
     */
    public function testPassesThroughUnmappedFile($baseUrl, $map, $requestUrl)
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())->map($baseUrl, $map);
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                $factory = new Psr17Factory();
                return $factory
                    ->createResponse()
                    ->withBody($factory->createStream('Hello World !'));
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $request = $creator->fromArrays([
            'REQUEST_METHOD' => RequestMethod::METHOD_GET,
            'REQUEST_URI' => $requestUrl
        ]);
        $response = $handler->process($request, $next);

        $this->assertEquals(StatusCode::STATUS_OK, $response->getStatusCode());
        $this->assertEquals('Hello World !', strval($response->getBody()));
    }

    /**
     * @dataProvider provideExistingsFiles
     */
    public function testFoundFile($baseUrl, $map, $requestUrl)
    {
        $factory = new Psr17Factory();

        $filepath = $map . '/' . basename($requestUrl);
        $options = (new StaticFilesOptions())->map($baseUrl, $map);
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $request = $creator->fromArrays([
            'REQUEST_METHOD' => RequestMethod::METHOD_GET,
            'REQUEST_URI' => $requestUrl
        ]);
        $response = $handler->process($request, $next);
        $content = file_get_contents($filepath);

        $this->assertEquals(StatusCode::STATUS_OK, $response->getStatusCode());
        $this->assertEquals('text/plain', $response->getHeaderLine('Content-Type'));
        $this->assertEquals(filesize($filepath), intval($response->getHeaderLine('Content-Length')));
        $this->assertEquals($response->getBody()->getSize(), intval($response->getHeaderLine('Content-Length')));
        $this->assertEquals($content, strval($response->getBody()));
    }

    /**
     * @dataProvider provideExistingsFiles
     */
    public function testHeadMethod($baseUrl, $map, $requestUrl)
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())->map($baseUrl, $map);
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse(404);
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $request = $creator->fromArrays([
            'REQUEST_METHOD' => RequestMethod::METHOD_HEAD,
            'REQUEST_URI' => $requestUrl
        ]);
        $response = $handler->process($request, $next);

        $this->assertEquals(StatusCode::STATUS_OK, $response->getStatusCode());
        $this->assertEquals('text/plain', $response->getHeaderLine('Content-Type'));
        $this->assertEquals(36, intval($response->getHeaderLine('Content-Length')));
        $this->assertEmpty(strval($response->getBody()));
    }

    /**
     * @dataProvider provideNotFoundFiles
     */
    public function testNotFoundFile($baseUrl, $map, $requestUrl)
    {
        $factory = new Psr17Factory();

        $options = (new StaticFilesOptions())->map($baseUrl, $map);
        $handler = new StaticFilesMiddleware($options, $factory, $factory);
        $next = new class() implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return (new Psr17Factory())->createResponse();
            }
        };

        $creator = new ServerRequestCreator($factory, $factory, $factory, $factory);
        $request = $creator->fromArrays([
            'REQUEST_METHOD' => RequestMethod::METHOD_HEAD,
            'REQUEST_URI' => $requestUrl
        ]);
        $response = $handler->process($request, $next);

        $this->assertEquals(StatusCode::STATUS_NOT_FOUND, $response->getStatusCode());
    }

    public function providePassesThroughRequests(): array
    {
        return [
            [ RequestMethod::METHOD_CONNECT, '/test.txt' ],
            [ RequestMethod::METHOD_DELETE, '/test.txt' ],
            [ RequestMethod::METHOD_OPTIONS, '/test.txt' ],
            [ RequestMethod::METHOD_PATCH, '/test.txt' ],
            [ RequestMethod::METHOD_POST, '/test.txt' ],
            [ RequestMethod::METHOD_PURGE, '/test.txt' ],
            [ RequestMethod::METHOD_PUT, '/test.txt' ],
            [ RequestMethod::METHOD_TRACE, '/test.txt' ],
            [ RequestMethod::METHOD_GET, '/something.unknown' ]
        ];
    }

    public function providePassesThroughFiles(): array
    {
        return [
            [ '/somewhere', __DIR__, '/nothing.txt' ],
            [ '/somewhere', __DIR__ . '/sub-folder', '/public/characters.txt' ]
        ];
    }

    public function provideExistingsFiles(): array
    {
        return [
            [ '/', __DIR__, '/test.txt' ],
            [ '/somewhere', __DIR__, '/somewhere/test.txt' ],
            [ '/somewhere', __DIR__, '/sOmEWhere/test.txt' ],
            [ '/', __DIR__ . '/sub-folder', '/characters.txt' ],
            [ '/somewhere', __DIR__ . '/sub-folder', '/somewhere/characters.txt' ]
        ];
    }

    public function provideNotFoundFiles(): array
    {
        return [
            [ '/', __DIR__, '/test.png' ],
            [ '/somewhere', __DIR__, '/somewhere/something.html' ],
            [ '/', __DIR__ . '/sub-folder', '/something.html' ],
            [ '/somewhere', __DIR__ . '/sub-folder', '/somewhere/something.html' ]
        ];
    }
}
