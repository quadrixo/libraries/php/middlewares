<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Middleware that redirects non-HTTPS requests to an HTTPS URL.
 */
class HttpsRedirectionMiddleware implements MiddlewareInterface
{
    private $statusCode;
    private $httpsPort;
    private $responseFactory;

    public function __construct(HttpsRedirectionOptions $options, ResponseFactoryInterface $responseFactory)
    {
        $this->statusCode = $options->getStatusCode();
        $this->httpsPort = $options->getHttpsPort();
        $this->responseFactory = $responseFactory;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $uri = $request->getUri();
        if ($uri->getScheme() == 'http')
        {
            $uri = $uri->withScheme('https');
            if ($this->httpsPort != 443)
            {
                $uri = $uri->withPort($this->httpsPort);
            }

            return $this->responseFactory
                ->createResponse($this->statusCode)
                ->withHeader('Location', "$uri");
        }

        return $handler->handle($request);
    }
}
