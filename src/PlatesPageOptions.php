<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares;

use Fig\Http\Message\RequestMethodInterface as RequestMethod;

class PlatesPageOptions
{
    public const DISALLOW_EXTENSION = 0x00;
    public const ALLOW_EXTENSION = 0x10;
    public const ALLOW_EXTENSION_WITH_REDIRECTION = 0x01;
    public const ALLOW_EXTENSION_PRESERVE_METHOD = 0x02;

    /** @var string[] list of allowed methods */
    private $allowedMethods = [ RequestMethod::METHOD_GET, RequestMethod::METHOD_HEAD, RequestMethod::METHOD_POST ];

    /** @var string the prefix of path to match */
    private $pathPrefix = '/';

    /** @var int if '.html' extension is accepted */
    private $allowExtension = PlatesPageOptions::DISALLOW_EXTENSION;

    /** @var string Namespace of page classes */
    private $namespace = 'Pages\\';

    public function getAllowedMethods(): array { return $this->allowedMethods; }
    public function getPathPrefix(): string { return $this->pathPrefix; }
    public function getExtensionAllowed(): int { return $this->allowExtension; }
    public function getNamespace(): string { return $this->namespace; }

    /**
     * Set the list of allowed method (default: GET, HEAD, POST)
     * @param string ...$methods
     * @return PlatesPageOptions
     */
    public function allowMethods(string ...$methods): PlatesPageOptions
    {
        $this->allowedMethods = array_map('strtoupper', $methods);
        return $this;
    }

    /**
     * Set the prefix to match against the path of the request.
     * @param string $pathPrefix
     * @return PlatesPageOptions
     */
    public function setPathPrefix(string $pathPrefix): PlatesPageOptions
    {
        assert(str_starts_with($pathPrefix, '/'));
        $this->pathPrefix = $pathPrefix;
        return $this;
    }

    /**
     * Allow the use of path with and without '.html' extension.
     *
     * If forceRediection is `true`, path without extension are redirect to
     * the path with the extension.
     *
     * @param boolean $forceRedirection
     * @return PlatesPageOptions
     */
    public function allowHtmlExtension(bool $forceRedirection = false, bool $preserveMethod = false): PlatesPageOptions
    {
        $this->allowExtension = PlatesPageOptions::ALLOW_EXTENSION
            | ($forceRedirection ? PlatesPageOptions::ALLOW_EXTENSION_WITH_REDIRECTION : 0x00)
            | ($preserveMethod ? PlatesPageOptions::ALLOW_EXTENSION_PRESERVE_METHOD : 0x00);
        return $this;
    }

    public function setNamespace(string $namespace): PlatesPageOptions
    {
        assert(!empty($namespace) && str_ends_with($namespace, '\\'));
        $this->namespace = $namespace;
        return $this;
    }
}
