<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares;

class HttpsRedirectionOptions
{
    /** @var int The status code used for the redirect response. The default is 307. */
    private $statusCode = 307;

    /** @var int The HTTPS port to be added to the redirected URL. */
    private $httpsPort = 443;

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function setStatusCode(int $statusCode): HttpsRedirectionOptions
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function getHttpsPort(): int
    {
        return $this->httpsPort;
    }

    public function setHttpsPort(int $httpsPort): HttpsRedirectionOptions
    {
        $this->httpsPort = $httpsPort;
        return $this;
    }
}
