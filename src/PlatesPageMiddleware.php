<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares;

use Fig\Http\Message\RequestMethodInterface as RequestMethod;
use Fig\Http\Message\StatusCodeInterface as StatusCode;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Quadrixo\Middlewares\Core\ResultInterface;
use Quadrixo\Middlewares\Core\Results\PlatesViewResult;
use Quadrixo\Middlewares\Internal\ObscureClassFactory;
use Quadrixo\Middlewares\PlatesPage\AbstractPage;
use UnexpectedValueException;

/**
 * Middleware which serves HTML pages.
 */
class PlatesPageMiddleware implements MiddlewareInterface
{
    /** @var string[] */
    private $allowedMethods;
    /** @var string */
    private $pathPrefix;
    /** @var int */
    private $allowExtension;
    /** @var string */
    private $namespace;
    /** @var ContainerInterface */
    private $container;
    /** @var ResponseFactoryInterface */
    private $responseFactory;

    public function __construct(PlatesPageOptions $options, ResponseFactoryInterface $responseFactory, ContainerInterface $container)
    {
        $this->allowedMethods = $options->getAllowedMethods();
        $this->pathPrefix = rtrim($options->getPathPrefix(), '/') ?: '/';
        $this->allowExtension = $options->getExtensionAllowed();
        $this->namespace = $options->getNamespace();
        $this->container = $container;
        $this->responseFactory = $responseFactory;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $this->render($request) ?: $handler->handle($request);
    }

    private function render(ServerRequestInterface $request): ?ResponseInterface
    {
        if (!in_array($request->getMethod(), $this->allowedMethods, false))
        {
            return null;
        }

        $path = $request->getUri()->getPath();
        if (!str_starts_with($path, $this->pathPrefix))
        {
            return null;
        }

        // Sanitize path and handle redirection
        $relativePath = substr($path, strlen($this->pathPrefix));
        if (str_ends_with($path, '/'))
        {
            $relativePath .= 'index';
        }
        else if (static::testFlag($this->allowExtension, PlatesPageOptions::ALLOW_EXTENSION))
        {
            if (str_ends_with($relativePath, '.htm'))
            {
                $relativePath = substr($relativePath, 0, -4);
            }
            else if (str_ends_with($relativePath, '.html'))
            {
                $relativePath = substr($relativePath, 0, -5);
            }
        }

        $result = $this->getResultFromClass($request, $relativePath) ?: $this->getViewResult($relativePath);

        if ($result === null)
        {
            return null;
        }
        else if (!str_ends_with($path, '/') && !str_ends_with($path, '.html') && static::testFlag($this->allowExtension, PlatesPageOptions::ALLOW_EXTENSION_WITH_REDIRECTION))
        {
            $status = static::testFlag($this->allowExtension, PlatesPageOptions::ALLOW_EXTENSION_PRESERVE_METHOD)
                ? StatusCode::STATUS_PERMANENT_REDIRECT : StatusCode::STATUS_MOVED_PERMANENTLY;

            return $this->responseFactory->createResponse($status)
                ->withHeader('Location', "$path.html");
        }

        return $result->execute($this->container, $request);
    }

    private function getResultFromClass(ServerRequestInterface $request, string $path): ?ResultInterface
    {
        $parts = explode('/', $path);
        $classname = $this->namespace . implode('\\', array_map('str_pascalize', $parts)) . 'Page';
        $viewname = "pages::$path";

        if (!$this->container->has($classname))
        {
            return null;
        }

        assert(is_subclass_of($classname, AbstractPage::class));

        $instance = ObscureClassFactory::getInstance($this->container, $classname, $request);
        $method = $request->getMethod();
        $action = strtolower($method) . 'Action';
        if (!method_exists($instance, $action) && $method == RequestMethod::METHOD_HEAD)
        {
            // Use get action instead of head action is not exists
            $action = 'getAction';
        }

        if (!method_exists($instance, $action))
        {
            throw new InvalidArgumentException("The page $classname has no action $action");
        }

        $result = call_user_func([ $instance, $action ]) ?: new PlatesViewResult($viewname, $instance->getData());

        if ($result instanceof ResultInterface)
        {
            return $result;
        }

        $type = get_debug_type($result);
        throw new UnexpectedValueException("$type is not a valid type for the return of an action");
    }

    private function getViewResult(string $path): ?ResultInterface
    {
        $viewname = "pages::$path";
        $result = new PlatesViewResult($viewname, []);

        return $result->exists($this->container) ? $result : null;
    }

    private static function testFlag(int $value, int $flag): bool
    {
        return ($value & $flag) == $flag;
    }
}
