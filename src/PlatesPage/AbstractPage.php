<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares\PlatesPage;

use BadMethodCallException;
use Fig\Http\Message\StatusCodeInterface as StatusCodes;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Quadrixo\Middlewares\Core\RequestHandlerTrait;
use Quadrixo\Middlewares\Core\ResultInterface;
use Quadrixo\Middlewares\Core\Results\{ ContentResult, PlatesViewResult, RedirectResult, StatusCodeResult, StreamResult };

abstract class AbstractPage
{
    use RequestHandlerTrait;

    private $data = [];

    /**
     * The container
     *
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * Add data to pass to the template
     *
     * @param string|array $key_or_data
     * @param mixed $value
     * @return void
     */
    public function addData($key_or_data, $value = null): void
    {
        $data = is_array($key_or_data) ? $key_or_data : [ $key_or_data => $value ];
        $this->data = $data + $this->data;
    }

    /**
     * Get all defined data
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Return a 400 Bad Request Response.
     */
    public function badRequest(?object $object = null): ResultInterface
    {
        if ($object == null)
        {
            return new StatusCodeResult(StatusCodes::STATUS_BAD_REQUEST);
        }
        return new ContentResult(json_encode($object), 'application/json', StatusCodes::STATUS_BAD_REQUEST);
    }

    /**
     * Return a response with the specified content.
     */
    public function content(string $content, ?string $contentType = null, ?int $statusCode = null): ResultInterface
    {
        return new ContentResult($content, $contentType, $statusCode);
    }

    /**
     * Return a file, wich is attached as a download file if `$downloadName` is specified.
     */
    public function file(string $path, string $contentType, ?string $downloadName = null, ?int $lastModified = null, ?string $etag = null): ResultInterface
    {
        /** @var StreamFactoryInterface */
        $factory = $this->getContainer()->get(StreamFactoryInterface::class);
        return new StreamResult(
            $factory->createStreamFromFile($path),
            $contentType,
            $downloadName,
            $lastModified,
            $etag,
            false
        );
    }

    /**
     * Return a 404 Not Found Response.
     */
    public function NotFound(?object $object = null): ResultInterface
    {
        if ($object == null)
        {
            return new StatusCodeResult(StatusCodes::STATUS_NOT_FOUND);
        }
        return new ContentResult(json_encode($object), 'application/json', StatusCodes::STATUS_NOT_FOUND);
    }

    /**
     * Return the plates page named `$name`.
     */
    public function page(string $name, ?int $statusCode = null, ?string $contentType = null): ResultInterface
    {
        return new PlatesViewResult($name, $this->getData(), $statusCode, $contentType);
    }

    /**
     * Return a 30X Redirection Response.
     */
    public function redirect(string $url, ?bool $permanent = null, ?bool $preserveMethod = null): ResultInterface
    {
        return new RedirectResult($url, $permanent, $preserveMethod);
    }

    /**
     * Return an empty response with the specified status code.
     */
    public function statusCode(int $statusCode): ResultInterface
    {
        return new StatusCodeResult($statusCode);
    }
}
