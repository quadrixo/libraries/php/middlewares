<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares;

class CorsOptions
{
    /** @var string[] */
    private $origins = [];
    /** @var string[] */
    private $headers = [];
    /** @var string[] */
    private $exposedHeaders = [];
    /** @var string[] */
    private $methods = [];
    /** @var bool */
    private $supportsCredentials = false;
    /** @var ?int */
    private $preflightMaxAge = null;

    public function getAllowedOrigins(): array
    {
        return $this->origins;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getExposedHeaders(): array
    {
        return $this->exposedHeaders;
    }

    public function getMethods(): array
    {
        return $this->methods;
    }

    public function getSupportsCredentials(): bool
    {
        return $this->supportsCredentials;
    }

    public function getPeflightMaxAge(): ?int
    {
        return $this->preflightMaxAge;
    }

    /**
     * Defines allowed origins
     *
     * @param array $origins
     * @return CorsOptions
     */
    public function withOrigins(string ...$origins): CorsOptions
    {
        $this->origins = array_merge($this->origins, $origins);
        return $this;
    }

    /**
     * Allows any origin
     *
     * @return CorsOptions
     */
    public function allowAnyOrigin(): CorsOptions
    {
        $this->origins = [ '*' ];
        return $this;
    }

    /**
     * Defines allowed headers
     *
     * @param array $headers
     * @return CorsOptions
     */
    public function withHeaders(string ...$headers): CorsOptions
    {
        $this->headers = array_merge($this->headers, $headers);
        return $this;
    }

    /**
     * Allows any header
     *
     * @return CorsOptions
     */
    public function allowAnyHeader(): CorsOptions
    {
        $this->headers = [ '*' ];
        return $this;
    }

    /**
     * Exposes headers to the client
     *
     * @param array $headers
     * @return CorsOptions
     */
    public function exposeHeaders(string ...$headers): CorsOptions
    {
        $this->exposedHeaders = array_merge($this->exposedHeaders, $headers);
        return $this;
    }

    /**
     * Defines allowed methods
     *
     * @param array $methods
     * @return CorsOptions
     */
    public function withMethods(string ...$methods): CorsOptions
    {
        $this->methods = array_merge($this->methods, $methods);
        return $this;
    }

    /**
     * Allows any methods
     *
     * @return CorsOptions
     */
    public function allowAnyMethods(): CorsOptions
    {
        $this->methods = [ '*' ];
        return $this;
    }

    /**
     * Allow credentials
     *
     * @return CorsOptions
     */
    public function allowCredentials(): CorsOptions
    {
        $this->supportsCredentials = true;
        return $this;
    }

    /**
     * Disallow credentials
     *
     * @return CorsOptions
     */
    public function disallowCredentials(): CorsOptions
    {
        $this->supportsCredentials = false;
        return $this;
    }

    /**
     * Set the preflight max age
     *
     * @param integer $preflightMaxAge
     * @return CorsOptions
     */
    public function setPreflightMaxAge(int $preflightMaxAge): CorsOptions
    {
        assert($preflightMaxAge > 0);

        $this->preflightMaxAge = $preflightMaxAge;
        return $this;
    }
}
