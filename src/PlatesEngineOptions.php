<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares;

class PlatesEngineOptions
{
    /** @var string extension of template files */
    private $extension = '.phtml';

    /** @var string the directory where to find shared templates */
    private $defaultDirectory = 'views/shared';

    /** @var string the directory where to find pages templates */
    private $pagesDirectory = 'views/pages';

    /** @var string the default layout of views */
    private $defaultLayout = 'layout';

    /** @var array registrered functions */
    private $registeredFunctions = [];

    /** @var array registrered extensions */
    private $registeredExtensions = [];

    public function getExtension(): string { return $this->extension; }
    public function getDefaultDirectory(): string { return $this->defaultDirectory; }
    public function getPagesDirectory(): string { return $this->pagesDirectory; }
    public function getDefaultLayout(): string { return $this->defaultLayout; }
    public function getRegisteredFunctions(): array { return $this->registeredFunctions; }
    public function getRegisteredExtensions(): array { return $this->registeredExtensions; }

    /**
     * Set the extension of template files.
     * @param string $extension
     * @return PlatesEngineOptions
     */
    public function setExtension(string $extension): PlatesEngineOptions
    {
        assert(str_starts_with($extension, '.'));
        $this->entension = $extension;
        return $this;
    }

    /**
     * Set the shared directory of templates
     *
     * @param string $defaultDirectory
     * @return PlatesEngineOptions
     */
    public function setDefaultDirectory(string $defaultDirectory): PlatesEngineOptions
    {
        assert(!str_starts_with($defaultDirectory, '/'));
        $this->defaultDirectory = $defaultDirectory;
        return $this;
    }

    /**
     * Set the directory for pages templates
     *
     * @param string $pagesDirectory
     * @return PlatesEngineOptions
     */
    public function setPagesDirectory(string $pagesDirectory): PlatesEngineOptions
    {
        assert(!str_starts_with($pagesDirectory, '/'));
        $this->pagesDirectory = $pagesDirectory;
        return $this;
    }

    /**
     * Set the default layout
     *
     * @param string $layoutName
     * @return PlatesEngineOptions
     */
    public function setDefaultLayout(string $layoutName): PlatesEngineOptions
    {
        $this->defaultLayout = $layoutName;
        return $this;
    }

    /**
     * Registers functions and extensions for templates of Plates engine.
     *
     * @param callable|ExtensionInterface ...$registeredFunctions The list of registeredFunctions
     * @return PlatesPageOptions
     */
    public function register(array $registered): PlatesEngineOptions
    {
        $this->registeredFunctions += array_filter($registered, 'is_callable');
        array_splice($this->registeredExtensions, count($this->registeredExtensions), 0, array_filter($registered, function($item) { return is_subclass_of($item, ExtensionInterface::class); }));
        return $this;
    }

    /**
     * Unregisters functions for templates of Plates engine.
     *
     * @param string[] $names The list of functions' name to unregister
     * @return PlatesPageOptions
     */
    public function unregister(string ...$names): PlatesEngineOptions
    {
        foreach ($names as $name)
        {
            $this->registeredFunctions[$name] = null;
        }
        return $this;
    }
}
