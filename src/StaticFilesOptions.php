<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares;

class StaticFilesOptions
{
    private $mappings = [];
    private $mappedMimeTypes = [];
    private $mimetypes = [];
    private $defaultMimeType = null;

    public function getMappings(): array
    {
        return $this->mappings;
    }

    public function getMappedMimeTypes(): array
    {
        return $this->mappedMimeTypes;
    }

    public function getMimeTypes(): array
    {
        return $this->mimetypes;
    }

    public function getDefaultMimeType(): ?string
    {
        return $this->defaultMimeType;
    }

    /**
     * Allow to override MimeType when mapping a file
     *
     * @param string $path
     * @param string $filepath
     * @param string $mimeType
     * @return StaticFilesOptions
     */
    public function mapFile(string $path, string $filepath, string $mimeType = null): StaticFilesOptions
    {
        $this->map($path, $filepath);
        if ($mimeType !== null)
        {
            $this->mappedMimeTypes[$path] = $mimeType;
        }
        return $this;
    }

    /**
     * Map a path from request URL to a physical directory.
     *
     * @param string $path
     * @param string $directory
     * @return StaticFilesOptions
     */
    public function map(string $path, string $directory): StaticFilesOptions
    {
        assert(str_starts_with($path, '/'), 'Relative path are not allowed');
        assert(!str_starts_with($directory, '/'), 'Absolute directory are not allowed');

        $this->mappings[$path] = $directory;
        return $this;
    }

    /**
     * Add custom content types, associating file extension to MIME type.
     *
     * ```php
     * // Adds new mime-type
     * $options->addMimeType('.dat', 'application/x-vnd-data');
     * // Replace an existing mime-type
     * $options->addMimeType('.ico', 'image/vnd.microsoft.icon');
     * ```
     * @param string $ext
     * @param string $mimetype
     * @return StaticFilesOptions
     */
    public function addMimeType(string $ext, string $mimetype): StaticFilesOptions
    {
        assert(str_starts_with($ext, '.'), 'Extensions should start with a dot');

        $this->mimetypes[$ext] = $mimetype;
        return $this;
    }

    /**
     * Remove an existing content type.
     *
     * ```php
     * $options->setMimeType('.png');
     * ```
     *
     * @param string $ext
     * @return StaticFilesOptions
     */
    public function removeMimeType(string $ext): StaticFilesOptions
    {
        assert(str_starts_with($ext, '.'), 'Extensions should start with a dot');

        $this->mimetypes[$ext] = null;
        return $this;
    }

    /**
     * Allow to serve files with unknown content type.
     *
     * @param string $defaultMimeType The default mime type
     * @return StaticFilesOptions
     */
    public function serveFileWithoutMimeType(string $defaultMimeType = 'application/octet-stream'): StaticFilesOptions
    {
        $this->defaultMimeType = $defaultMimeType;
        return $this;
    }
}
