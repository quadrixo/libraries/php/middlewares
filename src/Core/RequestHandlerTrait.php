<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares\Core;

use BadMethodCallException;
use Fig\Http\Message\RequestMethodInterface as RequestMethods;
use Psr\Http\Message\ServerRequestInterface;
use Quadrixo\Middlewares\Internal\HeadersHelper;

trait RequestHandlerTrait
{
    /**
     * Returns the current request
     *
     * @return ServerRequestInterface
     */
    public function getRequest(): ServerRequestInterface
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * Return the parsed body
     *
     * It handles content-types 'application/json', 'application/x-www-form-urlencoded'
     * and 'multipart/form-data'.
     *
     * @return array
     */
    public function getParsedBody(): array
    {
        $request = $this->getRequest();
        $header = explode(';', HeadersHelper::getSingle($request, 'Content-Type'), 2);
        $contentType = array_shift($header);
        $params = array_shift($header);

        if (strcmp($contentType, 'application/json') === 0)
        {
            $fd = fopen('php://input', 'rb');
            fseek($fd, 0, SEEK_SET);
            $content = stream_get_contents($fd);
            return json_decode($content, true);
        }

        if ($request->getMethod() == RequestMethods::METHOD_POST)
        {
            return $request->getParsedBody();
        }

        if (empty($contentType) || $contentType == 'application/x-www-form-urlencoded')
        {
            $body = $this->getRequest()->getBody();
            parse_str("$body", $data);
            return $data;
        }

        if (str_starts_with($contentType, 'multipart/form-data'))
        {
            list($_, $boundary) = explode('=', $params);
            return $this->parseMuiltipart($boundary);
        }

        throw new BadMethodCallException('Unexpected content type');
    }

    private function parseMuiltipart(string $boundary): array
    {
        $fd = fopen('php://input', 'rb');
        fseek($fd, 0, SEEK_SET);

        $newline = "\r\n";
        $separator = "--$boundary$newline";
        $end = "--$boundary--";
        $line = fgets($fd);
        while (!feof($fd))
        {
            if (strcmp($line, $separator) == 0)
            {
                $name = null;
                $value = '';

                $line = fgets($fd);
                $isFile = false;
                while (!feof($fd) && strcmp($line, $newline) != 0 && !$isFile)
                {
                    if (str_starts_with($line, 'Content-Disposition: form-data'))
                    {
                        $isFile = strpos($line, 'filename') !== false;
                        preg_match('/name="(?<name>[^"]+)"/', $line, $matches);
                        $name = $matches["name"];
                    }
                    $line = fgets($fd);
                }

                $line = fgets($fd);
                while (!feof($fd) && strcmp($line, $separator) != 0 && strcmp($line, $end) != 0)
                {
                    $value .= $line;
                    $line = fgets($fd);
                }

                if (!$isFile)
                {
                    $data[$name] = substr($value, 0, -2);
                }
            }
        }

        fclose($fd);

        return $data;
    }
}
