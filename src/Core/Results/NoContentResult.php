<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares\Core\Results;

use Fig\Http\Message\StatusCodeInterface as StatusCodes;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * A result which produces an empty HTTP response.
 */
class NoContentResult extends StatusCodeResult
{
    public function __construct()
    {
        parent::__construct(StatusCodes::STATUS_NO_CONTENT);
    }

    public function execute(ContainerInterface $container, ServerRequestInterface $request): ResponseInterface
    {
        return parent::execute($container, $request)
            ->withHeader('Content-Length', '0');
    }
}
