<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares\Core\Results;

use Fig\Http\Message\StatusCodeInterface as StatusCode;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Quadrixo\Middlewares\Core\ResultInterface;

/**
 * A result which produces an HTTP response with content.
 */
class ContentResult implements ResultInterface
{
    private const DEFAULT_CONTENT_TYPE = 'text/plain; charset=utf-8';

    private $content;
    private $contentType;
    private $statusCode;

    public function __construct(string $content, ?string $contentType = null, ?int $statusCode = null)
    {
        assert($content !== null);
        $this->content = $content;
        $this->contentType = $contentType;
        $this->statusCode = $statusCode;
    }

    public function execute(ContainerInterface $container, ServerRequestInterface $request): ResponseInterface
    {
        /** @var ResponseFactoryInterface */
        $responseFactory = $container->get(ResponseFactoryInterface::class);

        $response = $responseFactory->createResponse($this->statusCode ?: StatusCode::STATUS_OK)
            ->withHeader('Content-Type', $this->contentType ?: ContentResult::DEFAULT_CONTENT_TYPE);

        $response->getBody()->write($this->content);

        return $response;
    }
}
