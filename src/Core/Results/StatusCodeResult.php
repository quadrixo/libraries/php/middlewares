<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares\Core\Results;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Quadrixo\Middlewares\Core\ResultInterface;

/**
 * A result which produces an HTTP response with the given status code.
 */
class StatusCodeResult implements ResultInterface
{
    private $statusCode;

    public function __construct(int $statusCode)
    {
        assert(100 <= $statusCode && $statusCode <= 599);
        $this->statusCode = $statusCode;
    }

    public function execute(ContainerInterface $container, ServerRequestInterface $request): ResponseInterface
    {
        return $container
            ->get(ResponseFactoryInterface::class)
            ->createResponse($this->statusCode);
    }
}
