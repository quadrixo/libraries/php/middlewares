<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares\Core\Results;

use Fig\Http\Message\StatusCodeInterface as StatusCode;
use League\Plates\Engine;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Quadrixo\Middlewares\Core\ResultInterface;
use Quadrixo\Middlewares\Internal\PlatesEngineProvider;
use Quadrixo\Middlewares\PlatesEngineOptions;

class PlatesViewResult implements ResultInterface
{
    private const DEFAULT_CONTENT_TYPE = 'text/html; charset=utf-8';

    private $view;
    private $data;
    private $statusCode;
    private $contentType;

    public function __construct(string $view, ?array $data = null, ?int $statusCode = null, ?string $contentType = null)
    {
        $this->view = $view;
        $this->data = $data;
        $this->statusCode = $statusCode;
        $this->contentType = $contentType;
    }

    public function execute(ContainerInterface $container, ServerRequestInterface $request): ResponseInterface
    {
        $content = $this->getContent($container);
        $response = $this->getResponse($container);

        $response->getBody()->write($content);

        return $response;
    }

    public function exists(ContainerInterface $container): bool
    {
        return $this->getEngine($container)->exists($this->view);
    }

    private function getContent(ContainerInterface $container): string
    {
        /** @var PlatesEngineOptions */
        $options = $container->get(PlatesEngineOptions::class);
        $engine = $this->getEngine($container);

        $template = $engine->make($this->view);
        $template->data($this->data);
        $template->layout($options->getDefaultLayout() ?: null, $this->data);
        return $template->render();
    }

    private function getResponse(ContainerInterface $container): ResponseInterface
    {
        /** @var ResponseFactoryInterface */
        $responseFactory = $container->get(ResponseFactoryInterface::class);

        return $responseFactory->createResponse($this->statusCode ?: StatusCode::STATUS_OK)
            ->withHeader('Content-Type', $this->contentType ?: PlatesViewResult::DEFAULT_CONTENT_TYPE);
    }

    private function getEngine(ContainerInterface $container): Engine
    {
        return $container->get(PlatesEngineProvider::class)->provide($container);
    }
}
