<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares\Core\Results;

use Fig\Http\Message\RequestMethodInterface as RequestMethod;
use Fig\Http\Message\StatusCodeInterface as StatusCode;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;
use Quadrixo\Middlewares\Core\ResultInterface;
use Quadrixo\Middlewares\Internal\HeadersHelper;

/**
 * A result which produces an HTTP response with the given status code.
 */
class StreamResult implements ResultInterface
{
    private const UNSPECIFIED = 0;
    private const NOT_MODIFIED = 1;
    private const PROCESS = 2;
    private const FAILED = 3;

    /** @var StreamInterface */
    private $stream;
    private $contentType;
    private $downloadName;
    private $lastModified;
    private $etag;
    private $enableRange;

    public function __construct(
        StreamInterface $stream,
        string $contentType,
        ?string $downloadName = null,
        ?int $lastModified = null,
        ?string $etag = null,
        bool $enableRange = false)
    {
        $this->stream = $stream;
        $this->contentType = $contentType;
        $this->downloadName = $downloadName;
        $this->lastModified = $lastModified;
        $this->etag = $etag;
        $this->enableRange = $enableRange;
    }

    public function execute(ContainerInterface $container, ServerRequestInterface $request): ResponseInterface
    {
        /** @var ResponseInterface */
        $response = $container
            ->get(ResponseFactoryInterface::class)
            ->createResponse();

        if ($this->lastModified !== null)
        {
            $response = HeadersHelper::withDate($response, 'Last-Modified', $this->lastModified);
        }
        if ($this->etag !== null)
        {
            $response = $response->withHeader('ETag', $this->etag);
        }

        $state = max(...iterator_to_array($this->computeStates($request)));

        if ($state === static::FAILED)
        {
            return $response->withStatus(StatusCode::STATUS_PRECONDITION_FAILED);
        }
        if ($state === static::NOT_MODIFIED)
        {
            return $response->withStatus(StatusCode::STATUS_NOT_MODIFIED);
        }

        $response = $response->withHeader('Content-Type', $this->contentType);

        if ($this->downloadName !== null)
        {
            $response = $response->withHeader('Content-Disposition', "attachment; filename={$this->downloadName}");
        }

        /** @var StreamFactoryInterface */
        $streamFactory = $container->get(StreamFactoryInterface::class);

        $size = $this->stream->getSize();
        if ($size !== null)
        {
            $response = $response->withHeader('Content-Length', "$size");
            if ($this->enableRange)
            {
                return $this->createRangeResponse($request, $streamFactory, $response, $size, $state);
            }
        }

        $content = $response->getBody()->detach();
        $append = $this->stream->detach();
        stream_copy_to_stream($append, $content);
        $body = $streamFactory->createStreamFromResource($content);

        return $response->withBody($body);
    }

    private function createRangeResponse(
        ServerRequestInterface $request,
        StreamFactoryInterface $streamFactory,
        ResponseInterface $response,
        int $size,
        int $state
    ): ResponseInterface
    {
        $response = $response->withHeader('Accept-Ranges', 'bytes');

        if (
            in_array($request->getMethod(), [ RequestMethod::METHOD_HEAD, RequestMethod::METHOD_GET ])
            && in_array($state, [ static::UNSPECIFIED, static::PROCESS ])
            && $this->isIfRangeValid($request)
        )
        {
            if (HeadersHelper::tryParseRangeHeader($request, $size, $range))
            {
                if ($range === null)
                {
                    return $response->withStatus(StatusCode::STATUS_RANGE_NOT_SATISFIABLE)
                        ->withHeader('Content-Range', "*/$size")
                        ->withHeader('Content-Length', '0');
                }

                list($from, $to) = $range;
                $length = $to - $from + 1;

                $content = $response->getBody()->detach();
                $append = $this->stream->detach();
                stream_copy_to_stream($append, $content, $length, $from);

                $body = $streamFactory->createStreamFromResource($content);

                return $response->withStatus(StatusCode::STATUS_PARTIAL_CONTENT)
                    ->withHeader('Content-Range', "bytes $from-$to/$size")
                    ->withHeader('Content-Length', "$length")
                    ->withBody($body);
            }
        }
    }

    private function computeStates(ServerRequestInterface $request): iterable
    {
        // At least one value is returned
        yield static::UNSPECIFIED;

        if ($request->hasHeader('If-Match'))
        {
            $etags = $request->getHeader('If-Match');
            $match = true;
            while (!$match && key($etags) !== null)
            {
                $etag = current($etags);
                $match = $etag === '*' || $etag === $this->etag;
                next($etags);
            }
            yield $match ? static::PROCESS : static::FAILED;
        }

        if ($request->hasHeader('If-None-Match'))
        {
            $etags = $request->getHeader('If-None-Match');
            $match = true;
            while (!$match && key($etags) !== null)
            {
                $etag = current($etags);
                $match = $etag === '*' || $etag === $this->etag;
                next($etags);
            }
            yield $match ? static::NOT_MODIFIED : static::PROCESS;
        }

        $ifModifiedSince = HeadersHelper::parseDate($request, 'If-Modified-Since');
        if ($this->lastModified !== null && $ifModifiedSince !== null)
        {
            $modified = $ifModifiedSince <= time() && $ifModifiedSince < $this->lastModified;
            yield $modified ? static::PROCESS : static::NOT_MODIFIED;
        }

        $ifUnmodifiedSince = HeadersHelper::parseDate($request, 'If-Unmodified-Since');
        if ($this->lastModified !== null && $ifUnmodifiedSince !== null)
        {
            $unmodified = $ifUnmodifiedSince <= time() && $ifUnmodifiedSince >= $this->lastModified;
            yield $unmodified ? static::PROCESS : static::FAILED;
        }
    }

    private function isIfRangeValid(ServerRequestInterface $request)
    {
        if (!$request->hasHeader('If-Range'))
        {
            return true;
        }

        $time = HeadersHelper::parseDate($request, 'If-Range');
        if ($time !== null)
        {
            if ($this->lastModified !== null && $this->lastModified > $time)
            {
                return false;
            }
        }
        else if ($this->etag !== null && $this->etag !== HeadersHelper::getSingle($request, 'If-Range'))
        {
            return false;
        }

        return true;
    }
}
