<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares\Core\Results;

use Fig\Http\Message\StatusCodeInterface as StatusCode;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Quadrixo\Middlewares\Core\ResultInterface;

/**
 * A result which produces an HTTP response with the given status code.
 */
class RedirectResult implements ResultInterface
{
    private static $statusCodes = [
        0x00 => StatusCode::STATUS_FOUND,              // !$preserveMethod && !$permanent
        0x01 => StatusCode::STATUS_MOVED_PERMANENTLY,  // !$preserveMethod && $permanent
        0x10 => StatusCode::STATUS_TEMPORARY_REDIRECT, // $preserveMethod && !$permanent
        0x11 => StatusCode::STATUS_PERMANENT_REDIRECT  // $preserveMethod && $permanent
    ];

    private $url;
    private $statusCode;

    public function __construct(string $url, ?bool $permanent = null, ?bool $preserveMethod = null)
    {
        assert(!empty($url));
        $this->url = $url;
        $this->statusCode = RedirectResult::$statusCodes[intval($preserveMethod) << 1 | intval($permanent)];
    }

    public function execute(ContainerInterface $container, ServerRequestInterface $request): ResponseInterface
    {
        /** @var ResponseFactoryInterface */
        $factory = $container->get(ResponseFactoryInterface::class);
        return $factory->createResponse($this->statusCode)
            ->withHeader('Location', $this->url);
    }
}
