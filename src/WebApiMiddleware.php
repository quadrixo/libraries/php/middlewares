<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares;

use Closure;
use FastRoute\Dispatcher;
use FastRoute\Dispatcher\GroupCountBased;
use Fig\Http\Message\RequestMethodInterface as RequestMethods;
use Fig\Http\Message\StatusCodeInterface as StatusCode;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Quadrixo\Middlewares\Core\ResultInterface;
use Quadrixo\Middlewares\Core\Results\ContentResult;
use Quadrixo\Middlewares\Core\Results\NoContentResult;
use Quadrixo\Middlewares\Core\Results\StreamResult;
use Quadrixo\Middlewares\Internal\ObscureClassFactory;
use Quadrixo\Middlewares\WebApi\ApiControllerBase;
use Quadrixo\Middlewares\WebApi\WebApiRouter;
use ReflectionFunction;
use ReflectionFunctionAbstract;
use ReflectionMethod;
use ReflectionParameter;
use UnexpectedValueException;

/**
 */
class WebApiMiddleware implements MiddlewareInterface
{
    /** @var WebApiRouter */
    private $router;
    /** @var ContainerInterface */
    private $container;
    /** @var ResponseFactoryInterface */
    private $responseFactory;
    /** @var StreamFactoryInterface */
    private $streamFactory;

    public function __construct(WebApiRouter $router, ContainerInterface $container, ResponseFactoryInterface $responseFactory, StreamFactoryInterface $streamFactory)
    {
        $this->router = $router;
        $this->container = $container;
        $this->responseFactory = $responseFactory;
        $this->streamFactory = $streamFactory;
    }

    /** Process the request */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $this->dispatch($request) ?: $handler->handle($request);
    }

    private function dispatch(ServerRequestInterface $request): ?ResponseInterface
    {
        /** @var Dispatcher */
        $dispatcher = $this->container->has(Dispatcher::class)
            ? $this->container->get(Dispatcher::class)
            : new GroupCountBased($this->router->getData());

        $dispatch = $dispatcher->dispatch($request->getMethod(), $request->getUri()->getPath());

        $status = array_shift($dispatch);
        switch ($status)
        {
            case Dispatcher::NOT_FOUND:
                return null;
            case Dispatcher::METHOD_NOT_ALLOWED:
                $methods = array_shift($dispatch);
                return $this->responseFactory
                    ->createResponse(StatusCode::STATUS_METHOD_NOT_ALLOWED)
                    ->withHeader('Allow', $methods);
            case Dispatcher::FOUND:
                break;
            default:
                throw new UnexpectedValueException("'$status' is not a valid dispatch status value");
        }

        // Execute the handler
        list($handler, $data) = $dispatch;

        if ($handler instanceof Closure)
        {
            $result = $this->executeClosure($handler, $data);
        }
        else if (is_array($handler))
        {
            $result = $this->executeController($request, $handler, $data);
        }
        else if (is_string($handler) && is_subclass_of($handler, ApiControllerBase::class))
        {
            $method = $request->getMethod();
            $action = strtolower($method) . 'Action';

            if (!method_exists($handler, $action) && $method == RequestMethods::METHOD_HEAD)
            {
                // Use get action instead of head action is not exists
                $action = 'getAction';
            }

            if (!method_exists($handler, $action))
            {
                throw new InvalidArgumentException("The controller $handler has no action $action");
            }

            $result = $this->executeController($request, [$handler, $action], $data);
        }
        else
        {
            $type = get_debug_type($handler);
            throw new UnexpectedValueException("$type is not a valid type for a route handler");
        }

        // Handle the result
        if (is_string($result))
        {
            $result = new ContentResult($result);
        }
        else if (is_resource($result))
        {
            $result = new StreamResult($this->streamFactory->createStreamFromResource($result), 'application/octet-stream');
        }
        else if (is_object($result) && !($result instanceof ResultInterface))
        {
            $result = new ContentResult(json_encode($result), 'application/json');
        }
        else if ($result === null)
        {
            $result = new NoContentResult();
        }

        if ($result instanceof ResultInterface)
        {
            return $result->execute($this->container, $request);
        }

        $type = get_debug_type($result);
        throw new UnexpectedValueException("$type is not a valid type for the return of an action");
    }

    private function executeClosure(Closure $handler, array $data)
    {
        $parameters = $this->resolveArguments(new ReflectionFunction($handler), $data);
        return $handler->call($this->container, ...$parameters);
    }

    private function executeController(ServerRequestInterface $request, array $handler, array $data)
    {
        assert(count($handler) == 2 && is_subclass_of($handler[0], ApiControllerBase::class));

        $controller = ObscureClassFactory::getInstance($this->container, $handler[0], $request);
        $method = new ReflectionMethod(...$handler);
        $parameters = $this->resolveArguments($method, $data);
        return $method->invokeArgs($controller, $parameters);
    }

    private function resolveArguments(ReflectionFunctionAbstract $function, array $data): array
    {
        return array_map(function(ReflectionParameter $p) use($data)
        {
            return $data[$p->getName()] ?: null;
        }, $function->getParameters());
    }
}
