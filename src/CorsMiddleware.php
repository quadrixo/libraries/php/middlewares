<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares;

use Fig\Http\Message\StatusCodeInterface as StatusCodes;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Quadrixo\Middlewares\Internal\CorsPolicy;

class CorsMiddleware implements MiddlewareInterface
{
    /** @var CorsPolicy */
    private $policy;
    /** @var ResponseFactoryInterface */
    private $responseFactory;

    public function __construct(CorsOptions $options, ResponseFactoryInterface $responseFactory)
    {
        $this->policy = new CorsPolicy($options);
        $this->responseFactory = $responseFactory;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // Non-CORS request
        if (!$request->hasHeader('Origin'))
        {
            return $handler->handle($request);
        }

        list($isPreflight, $allowedOrigin, $headers, $varyByOrigin) = $this->policy->evaluate($request);

        if ($isPreflight)
        {
            $response = $this->responseFactory->createResponse(StatusCodes::STATUS_NO_CONTENT);
        }
        else
        {
            $response = $handler->handle($request);
        }

        if ($allowedOrigin !== null)
        {
            $headers = array_filter($headers);
            $response = array_reduce(
                array_map(function() { return func_get_args(); }, array_keys($headers), array_values($headers)),
                function($response, $header) { return $response->withHeader($header[0], $header[1]); },
                $response
            );

            if ($varyByOrigin)
            {
                $response = $response->withAddedHeader('Vary', 'Origin');
            }
        }

        return $response;
    }
}
