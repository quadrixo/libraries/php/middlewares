<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares\Internal;

use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ServerRequestInterface;

final class HeadersHelper
{
    private function __construct() { }

    /**
     * Retrieves a message header value by the given case-insensitive name.
     *
     * This method returns the first header value of the given
     * case-insensitive header name.
     *
     * If the header does not appear in the message, this method returns null.
     *
     * @param MessageInterface $message
     * @param string $name Case-insensitive header field name.
     * @return ?string
     */
    public static function getSingle(MessageInterface $message, string $name): ?string
    {
        return $message->hasHeader($name) ? $message->getHeader($name)[0] : null;
    }

    /**
     * Parse a date header.
     *
     * @param MessageInterface $message
     * @param string $name
     * @return integer|null
     */
    public static function parseDate(MessageInterface $message, string $name): ?int
    {
        if (!$message->hasHeader($name))
        {
            return null;
        }
        $time = strtotime($message->getHeader($name)[0]);
        return $time === false ? null : $time;
    }

    /**
     * Format the given timestamp to be used in a message header value.
     *
     * @param integer $date
     * @return string
     */
    public static function formatDate(int $date): string
    {
        return gmdate('D, d M Y H:i:s \G\M\T', $date);
    }

    /**
     * Return an instance with the provided value replacing the specified header.
     *
     * @param MessageInterface $message
     * @param string $name Case-insensitive header field name.
     * @param int $date The timestamp value.
     * @return A copy of `$message` with the given header.
     */
    public static function withDate(MessageInterface $message, string $name, int $date)
    {
        $strdate = static::formatDate($date);
        return $message->withHeader($name, $strdate, $date);
    }

    /**
     * Return an instance with the provided value replacing the specified header.
     *
     * @param MessageInterface $message
     * @param string $name Case-insensitive header field name.
     * @param int $date The timestamp value.
     * @return A copy of `$message` with the given header.
     */
    public static function withAddedDate(MessageInterface $message, string $name, int $date)
    {
        $strdate = static::formatDate($date);
        return $message->withAddedHeader($name, $strdate, $date);
    }

    /**
     * Try to parse range header.
     *
     * If a range header is defined but invalid, `$range` will be null whereas
     * it returns `true`.
     *
     * @param ServerRequestInterface $request
     * @param integer $length The max length of the entity
     * @param array $range The parsed range (ie: [ $from, $to ])
     * @return boolean If range header if set.
     */
    public static function tryParseRangeHeader(ServerRequestInterface $request, int $length, array &$range): bool
    {
        $range = null;
        if (!$request->hasHeader('Range'))
        {
            return false;
        }

        // Multiple ranges not supported
        $rawRange = $request->getHeaderLine('Range');
        if (empty($rawRange) || str_contains($rawRange, ','))
        {
            return false;
        }

        // invalid format (only 'bytes' unit is supported)
        if (!preg_match('/^bytes=(?P<from>\d*)-(?P<to>\d*)$/', $rawRange, $matches))
        {
            return false;
        }

        if ($length === 0)
        {
            return true;
        }

        if (is_numeric($matches['from']))
        {
            $from = intval($matches['from']);
            if ($from >= $length)
            {
                return true;
            }
            $to = is_numeric($matches['to']) ? intval($matches['to']) : $length - 1;
        }
        else if (is_numeric($matches['to']))
        {
            $to = intval($matches['to']);
            if ($to == 0)
            {
                return true;
            }

            $bytes = min($to, $length);
            $from = $length - $bytes;
            $to = $from + $bytes - 1;
        }
        else
        {
            // 'bytes=-' is invalid
            return false;
        }

        if ($to < $from)
        {
            $range = [ $to, $from ];
            return true;
        }
        return false;
    }
}
