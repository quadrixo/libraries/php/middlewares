<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares\Internal;

use League\Plates\Engine;
use Quadrixo\Middlewares\PlatesEngineOptions;

class PlatesEngineProvider
{
    /** @var PlatesEngineOptions */
    private $options;

    public function __construct(PlatesEngineOptions $options)
    {
        $this->options = $options;
    }

    public function provide(): Engine
    {
        $options = $this->options;
        $shared = $options->getDefaultDirectory();
        $pages = $options->getPagesDirectory();
        $extension = substr($options->getExtension(), 1);
        $engine = new Engine($shared, $extension);
        $engine->addFolder('shared', $shared);
        $engine->addFolder('pages', $pages);

        $functions = $this->options->getRegisteredFunctions();
        $extensions = $this->options->getRegisteredExtensions();
        foreach ($functions as $name => $callback)
        {
            if ($callback)
            {
                $engine->registerFunction($name, $callback);
            }
            else
            {
                $engine->dropFunction($name);
            }
        }
        $engine->loadExtensions($extensions);

        return $engine;
    }
}
