<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares\Internal;

use Fig\Http\Message\RequestMethodInterface as RequestMethods;
use InvalidArgumentException;
use Psr\Http\Message\ServerRequestInterface;
use Quadrixo\Middlewares\CorsOptions;

class CorsPolicy
{
    /** @var bool */
    private $allowAnyOrigin;
    /** @var string[] */
    private $allowedOrigins;
    /** @var string */
    private $allowedOriginsPattern;
    /** @var bool */
    private $allowAnyHeader;
    /** @var string[] */
    private $allowedHeaders;
    /** @var string[] */
    private $exposedHeaders;
    /** @var bool */
    private $allowAnyMethod;
    /** @var string[] */
    private $allowedMethods;
    /** @var bool */
    private $supportsCredentials;
    /** @var ?int */
    private $preflightMaxAge;

    public function __construct(CorsOptions $options)
    {
        // origins
        $this->allowedOrigins = $options->getAllowedOrigins();
        $this->allowAnyOrigin = count($this->allowedOrigins) == 1 && $this->allowedOrigins[0] === '*';
        if (!$this->allowAnyOrigin)
        {
            $patterns = array_map(
                function($origin) { return str_replace('*', '.*', $origin); },
                $this->allowedOrigins
            );

            $this->allowedOriginsPattern = '@^(' . implode('|', $patterns) . ')$@';
        }
        // headers
        $this->allowedHeaders = $options->getHeaders();
        $this->allowAnyHeader = count($this->allowedHeaders) == 1 && $this->allowedHeaders[0] === '*';
        // exposed headers
        $this->exposedHeaders = $options->getExposedHeaders();
        // methods
        $this->allowedMethods = $options->getMethods();
        $this->allowAnyMethod = count($this->allowedMethods) == 1 && $this->allowedMethods[0] === '*';
        // credentials
        $this->supportsCredentials = $options->getSupportsCredentials();
        // preflight max age
        $this->preflightMaxAge = $options->getPeflightMaxAge();
    }

    public function evaluate(ServerRequestInterface $request): array
    {
        if ($this->allowAnyOrigin && $this->supportsCredentials)
        {
            throw new InvalidArgumentException('The CORS protocol does not allow specifying a wildcard (any) origin and credentials at the same time. Configure the CORS policy by listing individual origins if credentials needs to be supported.');
        }

        $isPreflight = $request->getMethod() === RequestMethods::METHOD_OPTIONS && $request->hasHeader('Access-Control-Request-Method');

        $origin = $request->getHeader('origin')[0];
        $isAllowedOrigin = $this->isOriginAllowed($origin);

        $headers = [];
        $varyByOrigin = false;

        if ($isAllowedOrigin)
        {
            if ($isPreflight)
            {
                $headers += $this->getPreflightHeaders($request->getHeaderLine('Access-Control-Request-Method'), $request->getHeaderLine('Access-Control-Request-Headers'));
            }
            else if (!empty($this->exposedHeaders))
            {
                $headers += ['Access-Control-Expose-Headers' => implode(',', $this->exposedHeaders) ];
            }

            $headers += $this->getCommonHeaders($origin);

            $varyByOrigin = ($this->allowAnyOrigin && $this->supportsCredentials) || (!$this->allowAnyOrigin && count($this->allowedOrigins) > 1);
        }

        return [ $isPreflight, $isAllowedOrigin ? $headers['Access-Control-Allow-Origin'] : null, $headers, $varyByOrigin ];
    }

    private function isOriginAllowed(string $origin): bool
    {
        if (empty($origin))
        {
            return false;
        }

        if ($this->allowAnyOrigin)
        {
            return true;
        }

        return boolval(preg_match($this->allowedOriginsPattern, $origin));
    }

    private function getPreflightHeaders(string $requestMethods, string $requestHeaders): array
    {
        return [
            'Access-Control-Allow-Methods' => $this->allowAnyMethod ? $requestMethods : implode(',', $this->allowedMethods),
            'Access-Control-Allow-Headers' => $this->allowAnyHeader ? $requestHeaders : implode(',', $this->allowedHeaders),
            'Access-Control-Max-Age' => $this->preflightMaxAge
        ];
    }

    private function getCommonHeaders(string $origin): array
    {
        return [
            'Access-Control-Allow-Origin' => $this->allowAnyOrigin ? '*' : $origin,
            'Access-Control-Allow-Credentials' => $this->supportsCredentials ? 'true' : null
        ];
    }
}
