<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares\Internal;

use ContainerHelper;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionClass;

final class ObscureClassFactory
{
    private function __construct() {}

    public static function getInstance(ContainerInterface $container, string $classname, ServerRequestInterface $request)
    {
        /** @var object */
        $instance = null;
        eval(<<<EOT
            use Quadrixo\ContainerAwareTrait;
            use Quadrixo\Middlewares\Internal\RequestAwareTrait;

            \$instance = new class extends $classname {
                use ContainerAwareTrait;
                use RequestAwareTrait;

                public function __construct() {}
            };
        EOT);

        $class = new ReflectionClass($classname);
        $constructor = $class->getConstructor();

        if ($constructor)
        {
            ContainerHelper::execute($container, $constructor->getClosure($instance));
        }

        return $instance
            ->setContainer($container)
            ->setRequest($request);
    }
}
