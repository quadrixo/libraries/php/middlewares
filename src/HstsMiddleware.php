<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class HstsMiddleware implements MiddlewareInterface
{
    /** @var string[] */
    private $excludedHosts;

    /** @var string Strict-Transport-Security header */
    private $header;

    /**
     * Middleware that handle HTTP Strict Transport Security
     *
     * @link https://tools.ietf.org/html/rfc6797 RFC 6797 - HTTP Strict Transport Security (HSTS)
     *
     * @param HstsOptions $options middleware options
     */
    public function __construct(HstsOptions $options)
    {
        $maxAge = $options->getMaxAge();
        $includeSubdomains = $options->getIncludeSubDomains() ? '; includeSubDomains' : '';
        $preload = $options->getPreload() ? '; preload' : '';

        $this->header = "max-age=$maxAge$includeSubdomains$preload";
        $this->excludedHosts = $options->getExcludedHosts();
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);

        $uri = $request->getUri();
        if ($uri->getScheme() == 'https' && !$this->isHostExcluded($uri->getHost()))
        {
            $response = $response->withHeader('Strict-Transport-Security', $this->header);
        }
        return $response;
    }

    private function isHostExcluded(string $host): bool
    {
        if (empty($this->excludedHosts))
        {
            return false;
        }

        foreach ($this->excludedHosts as $excludedHost)
        {
            if (strcmp($excludedHost, $host) == 0)
            {
                return true;
            }
        }

        return false;
    }
}
