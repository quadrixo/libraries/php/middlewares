<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares\WebApi;

use FastRoute\{ RouteCollector, RouteParser, DataGenerator };
use FastRoute\DataGenerator\GroupCountBased;
use FastRoute\RouteParser\Std;
use Fig\Http\Message\RequestMethodInterface as RequestMethods;
use Psr\Container\ContainerInterface;

class WebApiRouter extends RouteCollector
{
    private const ALL_VERBS = [
        RequestMethods::METHOD_GET,
        RequestMethods::METHOD_HEAD,
        RequestMethods::METHOD_POST,
        RequestMethods::METHOD_PUT,
        RequestMethods::METHOD_DELETE,
        RequestMethods::METHOD_CONNECT,
        RequestMethods::METHOD_OPTIONS,
        RequestMethods::METHOD_TRACE,
        RequestMethods::METHOD_PATCH
    ];

    public function __construct(ContainerInterface $container)
    {
        $routeParser = $container->has(RouteParser::class) ? $container->get(RouteParser::class) : new Std();
        $dataGenerator = $container->has(DataGenerator::class) ? $container->get(DataGenerator::class) : new GroupCountBased();
        parent::__construct($routeParser, $dataGenerator);
    }

    public function all($route, $handler)
    {
        $this->addRoute(WebApiRouter::ALL_VERBS, $route, $handler);
    }
}
