<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares\WebApi;

use BadMethodCallException;
use Fig\Http\Message\StatusCodeInterface as StatusCodes;
use Psr\Container\ContainerInterface;
use Quadrixo\Middlewares\Core\RequestHandlerTrait;
use Quadrixo\Middlewares\Core\ResultInterface;
use Quadrixo\Middlewares\Core\Results\ContentResult;
use Quadrixo\Middlewares\Core\Results\NoContentResult;
use Quadrixo\Middlewares\Core\Results\RedirectResult;
use Quadrixo\Middlewares\Core\Results\StatusCodeResult;
use Quadrixo\Middlewares\Core\Results\StreamResult;
use Throwable;

class ApiControllerBase
{
    use RequestHandlerTrait;

    /**
     * The container
     *
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * Return a 200 OK response with the given object serialized in JSON.
     */
    public function ok(object $object): ResultInterface
    {
        return new ContentResult(json_encode($object), 'application/json');
    }

    /**
     * Return a 400 Bad Request Response.
     */
    public function badRequest(?object $object = null): ResultInterface
    {
        if ($object == null)
        {
            return new StatusCodeResult(StatusCodes::STATUS_BAD_REQUEST);
        }
        return new ContentResult(json_encode($object), 'application/json', StatusCodes::STATUS_BAD_REQUEST);
    }

    /**
     * Return a 500 Internal Server Error Response.
     */
    public function problem(?object $object = null): ResultInterface
    {
        if ($object == null)
        {
            return new StatusCodeResult(StatusCodes::STATUS_INTERNAL_SERVER_ERROR);
        }
        else if ($object instanceof Throwable)
        {
            $object = new class($object)
            {
                public $message;
                public $code;
                public $file;
                public $line;
                public $trace;

                public function __construct(Throwable $error)
                {
                    $this->message = $error->getMessage();
                    $this->code = $error->getCode();
                    $this->file = $error->getFile();
                    $this->line = $error->getLine();
                    $this->trace = $error->getTraceAsString();
                }
            };
        }
        return new ContentResult(json_encode($object), 'application/json', StatusCodes::STATUS_INTERNAL_SERVER_ERROR);
    }

    /**
     * Return a response with the specified content.
     */
    public function content(string $content, ?string $contentType = null, ?int $statusCode = null): ResultInterface
    {
        return new ContentResult($content, $contentType, $statusCode);
    }

    /**
     * Return a response with no content.
     */
    public function noContent(): ResultInterface
    {
        return new NoContentResult();
    }

    /**
     * Return a file, wich is attached as a download file if `$downloadName` is specified.
     */
    public function file(string $path, string $contentType, ?string $downloadName = null, ?int $lastModified = null, ?string $etag = null): ResultInterface
    {
        /** @var StreamFactoryInterface */
        $factory = $this->getContainer()->get(StreamFactoryInterface::class);
        return new StreamResult(
            $factory->createStreamFromFile($path),
            $contentType,
            $downloadName,
            $lastModified,
            $etag,
            false
        );
    }

    /**
     * Return a 404 Not Found Response.
     */
    public function notFound(?object $object = null): ResultInterface
    {
        if ($object == null)
        {
            return new StatusCodeResult(StatusCodes::STATUS_NOT_FOUND);
        }
        return new ContentResult(json_encode($object), 'application/json', StatusCodes::STATUS_NOT_FOUND);
    }

    /**
     * Return a 30X Redirection Response.
     */
    public function redirect(string $url, ?bool $permanent = null, ?bool $preserveMethod = null): ResultInterface
    {
        return new RedirectResult($url, $permanent, $preserveMethod);
    }

    /**
     * Return an empty response with the specified status code.
     */
    public function statusCode(int $statusCode): ResultInterface
    {
        return new StatusCodeResult($statusCode);
    }
}
