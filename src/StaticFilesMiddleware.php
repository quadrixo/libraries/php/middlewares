<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares;

use Fig\Http\Message\RequestMethodInterface as RequestMethod;
use Fig\Http\Message\StatusCodeInterface as StatusCode;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Middleware which serves static files.
 *
 * Mappings between the path in the URL and the directory are declared in
 * `Quadrixo\Mioddlewares\StaticFileOptions`.
 */
class StaticFilesMiddleware implements MiddlewareInterface
{
    private const STATE_UNSPECIFIED    = 0;
    private const STATE_NOT_MODIFIED   = 1;
    private const STATE_SHOULD_PROCESS = 2;
    private const STATE_FAILED         = 3;

    /** @var string[] */
    private const ALLOWED_METHODS = [ RequestMethod::METHOD_GET, RequestMethod::METHOD_HEAD ];

    /** @var array<string, string> */
    private $mapping;

    /** @var array<string, string> */
    private $mappedMimeTypes;

    /** @var array<string, string> */
    private $mimetypes;

    /** @var ?string */
    private $defaultMimeType;

    /** @var ResponseFactoryInterface */
    private $responseFactory;

    /** @var StreamFactoryInterface */
    private $streamFactory;

    /** Constructor */
    public function __construct(
        StaticFilesOptions $options,
        ResponseFactoryInterface $responseFactory,
        StreamFactoryInterface $streamFactory)
    {
        $mappings = $options->getMappings();
        $this->mapping = array_combine(
            array_map('strtolower', array_keys($mappings)),
            array_values($mappings)
        );
        $this->mappedMimeTypes = $options->getMappedMimeTypes();
        $this->mimetypes = $options->getMimeTypes();
        $this->defaultMimeType = $options->getDefaultMimeType();
        $this->responseFactory = $responseFactory;
        $this->streamFactory = $streamFactory;
    }

    /** Process the request */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $this->tryServe($request) ?? $handler->handle($request);
    }

    private function tryServe(ServerRequestInterface $request): ?ResponseInterface
    {
        if (!in_array($request->getMethod(), StaticFilesMiddleware::ALLOWED_METHODS))
        {
            return null;
        }

        $uriPath = $request->getUri()->getPath();
        $contentType = isset($this->mappedMimeTypes[$uriPath]) ? $this->mappedMimeTypes[$uriPath] : $this->resolveMimeType($uriPath);
        if (!$contentType)
        {
            return null;
        }

        $path = $this->resolvePath($request);
        if (!$path)
        {
            return null;
        }

        $filename = stream_resolve_include_path($path);
        if (!$filename || !is_file($filename))
        {
            return $this->createStatusResponse(StatusCode::STATUS_NOT_FOUND, []);
        }

        $now = time();
        $length = filesize($path);
        $lastModified = filemtime($path);
        $etag = '"' . dechex($lastModified ^ $length) . '"';

        $ifMatchState = $this->computeIfMatch(
            $request->getHeader('If-Match'),
            $etag
        );

        $ifNoneMatch = $this->computeIfNoneMatch(
            $request->getHeader('If-None-Match'),
            $etag
        );

        $ifModifiedSinceState = $this->computeIfModifiedSince(
            $request->getHeaderLine('If-Modified-Since'),
            $now,
            $lastModified
        );

        $ifUnmodifiedSinceState = $this->computeIfUnmodifiedSince(
            $request->getHeaderLine('If-Unmodified-Since'),
            $now,
            $lastModified
        );

        $state = max($ifMatchState, $ifNoneMatch, $ifModifiedSinceState, $ifUnmodifiedSinceState);

        $context = compact('contentType', 'etag', 'lastModified', 'length');

        switch ($state)
        {
            case static::STATE_FAILED:
                return $this->createStatusResponse(StatusCode::STATUS_PRECONDITION_FAILED, $context);
            case static::STATE_NOT_MODIFIED:
                return $this->createStatusResponse(StatusCode::STATUS_NOT_MODIFIED, $context);
            case static::STATE_SHOULD_PROCESS:
            case static::STATE_UNSPECIFIED:
            default:
                break;
        }

        if ($request->getMethod() === RequestMethod::METHOD_HEAD)
        {
            return $this->createStatusResponse(StatusCode::STATUS_OK, $context);
        }

        $body = $this->streamFactory->createStreamFromFile($path);

        if (!$this->tryParseRange($request, $context, $range))
        {
            return $this->createStatusResponse(StatusCode::STATUS_OK, $context)
                ->withBody($body);
        }

        if (is_null($range))
        {
            return $this->createStatusResponse(StatusCode::STATUS_RANGE_NOT_SATISFIABLE, $context)
                ->withHeader('Content-Range', "*/$length");
        }

        list($from, $to) = $range;
        $size = $to - $from + 1;
        $body->seek($from);
        $content = $body->read($size);
        return $this->createStatusResponse(StatusCode::STATUS_PARTIAL_CONTENT, $context)
            ->withHeader('Range-Content', "bytes $from-$to/$length")
            ->withHeader('Content-Lenght', "$size")
            ->withBody($this->streamFactory->createStream($content));
    }

    /**
     * @param string[] $ifMatch
     * @param string $etag
     * @return void
     */
    private function computeIfMatch(array $ifMatch, string $etag): int
    {
        if (empty($ifMatch))
        {
            return static::STATE_UNSPECIFIED;
        }

        foreach ($ifMatch as $match)
        {
            if ($match == '*' || $match === $etag)
            {
                return static::STATE_SHOULD_PROCESS;
            }
        }

        return static::STATE_FAILED;
    }

    /**
     * @param string[] $ifNoneMatch
     * @param string $etag
     * @return void
     */
    private function computeIfNoneMatch(array $ifNoneMatch, string $etag): int
    {
        if (empty($ifNoneMatch))
        {
            return static::STATE_UNSPECIFIED;
        }

        foreach ($ifNoneMatch as $match)
        {
            if ($match == '*' || $match === $etag)
            {
                return static::STATE_NOT_MODIFIED;
            }
        }

        return static::STATE_SHOULD_PROCESS;
    }


    private function computeIfModifiedSince(string $ifModifiedSince, int $now, int $lastModified): int
    {
        $time = strtotime($ifModifiedSince);
        if ($time && $time <= $now)
        {
            $modified = $time < $lastModified;
            return $modified ? static::STATE_SHOULD_PROCESS : static::STATE_NOT_MODIFIED;
        }

        return static::STATE_UNSPECIFIED;
    }

    private function computeIfUnmodifiedSince(string $ifUnmodifiedSince, int $now, int $lastModified): int
    {
        $time = strtotime($ifUnmodifiedSince);
        if ($time && $time <= $now)
        {
            $unmodified = $time >= $lastModified;
            return $unmodified ? static::STATE_SHOULD_PROCESS : static::STATE_FAILED;
        }

        return static::STATE_UNSPECIFIED;
    }

    /**
     * ```php
     * $context = [
     *     'contentType' => $contentType,
     *     'etag' => $etag,
     *     'lastModified' => $lastModified,
     *     'length' => $length
     * ];
     * ```
     */
    private function tryParseRange(ServerRequestInterface $request, array $context, &$range): bool
    {
        $range = null;
        if (!$request->hasHeader('Range'))
        {
            return false;
        }

        extract($context);

        $rawRange = $request->getHeaderLine('Range');
        if (empty($rawRange))
        {
            return false;
        }
        // Multiple ranges not supported
        if (str_contains($rawRange, ','))
        {
            return false;
        }
        // invalid format (only 'bytes' unit is supported)
        if (!preg_match('/^bytes=(?P<from>\d*)-(?P<to>\d*)$/', $rawRange, $matches))
        {
            return false;
        }

        if (is_numeric($matches['from']))
        {
            $from = intval($matches['from']);
            if ($from >= $length)
            {
                return true;
            }
            $to = is_numeric($matches['to']) ? intval($matches['to']) : $length - 1;
        }
        else if (is_numeric($matches['to']))
        {
            $to = intval($matches['to']);
            if ($to == 0)
            {
                return true;
            }

            $bytes = min($to, $length);
            $from = $length - $bytes;
            $to = $from + $bytes - 1;
        }
        else
        {
            // 'bytes=-' is invalid
            return true;
        }

        if ($to < $from)
        {
            return true;
        }

        if ($request->hasHeader('If-Range'))
        {
            $rTime = strtotime($request->getHeaderLine('If-Range'));
            $rEtag = !$rTime ? $request->getHeaderLine('If-Range') : false;

            if ($rTime && $lastModified > $rTime)
            {
                return false;
            }
            if ($rEtag && $etag !== $rEtag)
            {
                return false;
            }
        }

        $range = [ $from, $to ];
        return true;
    }

    /**
     * Create the response with headers but without body.
     *
     * ```php
     * $context = [
     *     'contentType' => $contentType,
     *     'etag' => $etag,
     *     'lastModified' => $lastModified,
     *     'length' => $length
     * ];
     * ```
     *
     * @param integer $code
     * @param array $context
     * @return ResponseInterface
     */
    private function createStatusResponse(int $code, array $context): ResponseInterface
    {
        extract($context);
        $response = $this->responseFactory->createResponse($code);
        if ($code < 400)
        {
            $response = $response->withHeader('Content-Type', $contentType)
                ->withHeader('Last-Modified', gmdate('D, d M Y H:i:s \G\M\T', $lastModified))
                ->withHeader('ETag', $etag);
        }
        if ($code == StatusCode::STATUS_OK)
        {
            $response = $response->withHeader('Content-Length', $length);
        }
        return $response;
    }

    /**
     * Resolve the file path
     *
     * This method doesn't ensure that the resolved path is a regular file.
     *
     * @param ServerRequestInterface $request
     * @return ?string
     */
    private function resolvePath(ServerRequestInterface $request): ?string
    {
        $path = $request->getUri()->getPath();
        $filename = '';

        while ($path != '/' && !$this->isMapped($path))
        {
            $filename = '/' . basename($path) . $filename;
            $pos = strrpos($path, '/');
            $path = $pos == 0 ? '/' : substr($path, 0, $pos);
        }

        if ($this->isMapped($path))
        {
            $filename = $this->getMap($path) . $filename;
        }
        else
        {
            $filename = substr($filename, 1);
            $filename = stream_resolve_include_path($filename);
        }

        return $filename ? $filename : null;
    }

    private function isMapped(string $path)
    {
        return isset($this->mapping[strtolower($path)]);
    }

    private function getMap(string $path)
    {
        return $this->mapping[strtolower($path)];
    }

    /**
     * Returns the mime type for the given filename
     *
     * @param string $filename
     * @return string|null
     */
    private function resolveMimeType(string $filename): ?string
    {
        $mimetypes = $this->mimetypes + static::$defaultMimeTypes;
        $ext = basename($filename);
        $offset = 0;
        for ($pos = strpos($ext, '.', $offset); $pos !== false; $pos = strpos($ext, '.', $offset))
        {
            $extension = substr($ext, $pos);
            if (isset($mimetypes[$extension]))
            {
                return $mimetypes[$extension];
            }
            $offset = $pos + 1;
        }

        return $this->defaultMimeType;
    }

    private static $defaultMimeTypes = [
        '.3g2' => 'video/3gpp2',
        '.3gp' => 'video/3gpp',
        '.7z' => 'application/x-7z-compressed',
        '.aac' => 'audio/aac',
        '.abw' => 'application/x-abiword',
        '.arc' => 'application/octet-stream',
        '.asc' => 'text/plain',
        '.asf' => 'video/x-ms-asf',
        '.asx' => 'video/x-ms-asf',
        '.avi' => 'video/x-msvideo',
        '.azw' => 'application/vnd.amazon.ebook',
        '.bin' => 'application/octet-stream',
        '.bmp' => 'image/bmp',
        '.bz2' => 'application/x-bzip',
        '.bz2' => 'application/x-bzip2',
        '.bz' => 'application/x-bzip',
        '.class' => 'application/octet-stream',
        '.conf' => 'text/plain',
        '.cpp' => 'text/plain',
        '.csh' => 'application/x-csh',
        '.css' => 'text/css',
        '.csv' => 'text/csv',
        '.c' => 'text/plain',
        '.doc' => 'application/msword',
        '.docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        '.dtd' => 'text/xml',
        '.dvi' => 'application/x-dvi',
        '.eot' => 'application/vnd.ms-fontobject',
        '.epub' => 'application/epub+zip',
        '.gif' => 'image/gif',
        '.gz' => 'application/x-gzip',
        '.html' => 'text/html',
        '.htm' => 'text/html',
        '.ico' => 'image/x-icon',
        '.ics' => 'text/calendar',
        '.jar' => 'application/java-archive',
        '.jpeg' => 'image/jpeg',
        '.jpg' => 'image/jpeg',
        '.js' => 'application/javascript',
        '.json' => 'application/json',
        '.js' => 'text/javascript',
        '.log' => 'text/plain',
        '.m3u' => 'audio/x-mpegurl',
        '.mid' => 'audio/midi',
        '.midi' => 'audio/midi',
        '.mov' => 'video/quicktime',
        '.mp3' => 'audio/mpeg',
        '.mpeg' => 'video/mpeg',
        '.mpg' => 'video/mpeg',
        '.mpkg' => 'application/vnd.apple.installer+xml',
        '.odc' => 'application/vnd.oasis.opendocument.chart',
        '.odf' => 'application/vnd.oasis.opendocument.formula',
        '.odg' => 'application/vnd.oasis.opendocument.graphics',
        '.odi' => 'application/vnd.oasis.opendocument.image',
        '.odm' => 'application/vnd.oasis.opendocument.text-master',
        '.odp' => 'application/vnd.oasis.opendocument.presentation',
        '.ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        '.odt' => 'application/vnd.oasis.opendocument.text',
        '.oga' => 'audio/ogg',
        '.ogg' => 'application/ogg',
        '.ogv' => 'video/ogg',
        '.ogx' => 'application/ogg',
        '.otc' => 'application/vnd.oasis.opendocument.chart-template',
        '.otf' => 'application/vnd.oasis.opendocument.formula-template',
        '.otf' => 'font/otf',
        '.otg' => 'application/vnd.oasis.opendocument.graphics-template',
        '.oth' => 'application/vnd.oasis.opendocument.text-web',
        '.oti' => 'application/vnd.oasis.opendocument.image-template',
        '.otp' => 'application/vnd.oasis.opendocument.presentation-template',
        '.ots' => 'application/vnd.oasis.opendocument.spreadsheet-template',
        '.ott' => 'application/vnd.oasis.opendocument.text-template',
        '.pac' => 'application/x-ns-proxy-autoconfig',
        '.pdf' => 'application/pdf',
        '.png' => 'image/png',
        '.ppt' => 'application/vnd.ms-powerpoint',
        '.pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        '.ps' => 'application/postscript',
        '.qt' => 'video/quicktime',
        '.rar' => 'application/x-rar-compressed',
        '.rtf' => 'application/rtf',
        '.sh' => 'application/x-sh',
        '.sig' => 'application/pgp-signature',
        '.spec' => 'text/plain',
        '.spl' => 'application/futuresplash',
        '.svg' => 'image/svg+xml',
        '.swf' => 'application/x-shockwave-flash',
        '.tar' => 'application/x-tar',
        '.tar.bz2' => 'application/x-bzip-compressed-tar',
        '.tar.gz' => 'application/x-tgz',
        '.tbz' => 'application/x-bzip-compressed-tar',
        '.text' => 'text/plain',
        '.tgz' => 'application/x-tgz',
        '.tiff' => 'image/tiff',
        '.tif' => 'image/tiff',
        '.torrent' => 'application/x-bittorrent',
        '.ts' => 'application/typescript',
        '.ttf' => 'font/ttf',
        '.txt' => 'text/plain',
        '.vsd' => 'application/vnd.visio',
        '.wav' => 'audio/x-wav',
        '.wax' => 'audio/x-ms-wax',
        '.weba' => 'audio/webm',
        '.webm' => 'video/webm',
        '.webp' => 'image/webp',
        '.wma' => 'audio/x-ms-wma',
        '.wmv' => 'video/x-ms-wmv',
        '.woff2' => 'font/woff2',
        '.woff' => 'font/woff',
        '.xbm' => 'image/x-xbitmap',
        '.xhtml' => 'application/xhtml+xml',
        '.xls' => 'application/vnd.ms-excel',
        '.xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        '.xml' => 'application/xml',
        '.xml' => 'text/xml',
        '.xpm' => 'image/x-xpixmap',
        '.xul' => 'application/vnd.mozilla.xul+xml',
        '.xwd' => 'image/x-xwindowdump',
        '.zip' => 'application/zip'
    ];
}
