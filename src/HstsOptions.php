<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/middlewares library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\Middlewares;

class HstsOptions
{
    /** @var int The max-age parameter in seconds of the Strict-Transport-Security header */
    private $maxAge;

    /** @var bool Enables includeSubDomain parameter of the Strict-Transport-Security header */
    private $includeSubDomains;

    /** @var bool Sets the preload parameter of the Strict-Transport-Security header */
    private $preload;

    /** @var string[] A list of host names that will not add the HSTS header */
    private $excludedHosts;

    public function __construct()
    {
        $this->maxAge = 30 * 24 * 60 * 60;
        $this->includeSubDomains = false;
        $this->preload = false;
        $this->excludedHosts = [ 'localhost', '127.0.0.1', '[::1]' ];
    }

    public function getMaxAge(): int
    {
        return $this->maxAge;
    }

    public function setMaxAge(int $maxAge): HstsOptions
    {
        $this->maxAge = $maxAge;
        return $this;
    }

    public function getIncludeSubDomains(): bool
    {
        return $this->includeSubDomains;
    }

    public function setIncludeSubDomains(bool $includeSubDomains): HstsOptions
    {
        $this->includeSubDomains = $includeSubDomains;
        return $this;
    }

    public function getPreload(): bool
    {
        return $this->preload;
    }

    public function setPreload(bool $preload): HstsOptions
    {
        $this->preload = $preload;
        return $this;
    }

    public function getExcludedHosts(): array
    {
        return $this->excludedHosts;
    }

    public function setExcludedHosts(array $excludedHosts): HstsOptions
    {
        $this->excludedHosts = array_values($excludedHosts);
        return $this;
    }

    /**
     * Add hosts to excluded host list.
     *
     * @param string|array $excludedHosts
     * @return HstsOptions
     */
    public function addExcludedHosts($excludedHosts): HstsOptions
    {
        if (is_string($excludedHosts))
        {
            $excludedHosts = [ $excludedHosts ];
        }
        array_splice($this->excludedHosts, count($this->excludedHosts), 0, array_values($excludedHosts));
        return $this;
    }
}
